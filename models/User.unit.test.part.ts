import User from "./User";

export default () => describe("User", () => {
    it("multiple can be created", async() => {
        await expect(User.create(User.fakeData())).resolves.not.toThrow();
        await expect(User.create(User.fakeData())).resolves.not.toThrow();
    });
    it("cannot be created with same email", async() => {
        const user1 = User.fakeData();
        const user2 = User.fakeData();
        user2.email = user1.email;
        await expect(User.create(user1)).resolves.not.toThrow();
        await expect(User.create(user2)).rejects.toThrow();
    });
});
