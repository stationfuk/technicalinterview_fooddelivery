import faker from "faker";
import Meal from "./Meal";
import Order from "./Order";
import { IUser } from "./User";
import mongoose, { Document, Model, Schema, Types } from "mongoose";
import ObjectId = mongoose.Types.ObjectId;

export interface IRestaurant extends Document {
    name: string;
    description: string;
    image: string;

    blockedUsers: (string | mongoose.Types.ObjectId | IUser)[];

    owner: string | mongoose.Types.ObjectId | IUser;
}

export interface RestaurantModel extends Model<IRestaurant> {
    createRestaurant(data: {
        name: string;
        description: string;
        image: string;
        owner: string | ObjectId | IUser;
        blockedUsers?: (string | ObjectId | IUser)[];
    }): Promise<IRestaurant>;
    updateRestaurant(data: {
        id: string;
        name: string;
        description: string;
        image: string;
    }): Promise<void>;
    fakeData(): {
        name: string;
        description: string;
        image: string;
    };
}

const RestaurantSchema = new Schema<IRestaurant, RestaurantModel>({
    name: {
        type: String,
        unique: true,
        index: true,
        required: [true, "Please provide a name."],
        maxlength: [50, "Name cannot be longer than 50 characters."]
    },
    description: {
        type: String,
        required: [true, "Please provide a description."],
        maxlength: [250, "Description cannot be longer than 250 characters."]
    },
    image: {
        type: String
    },
    blockedUsers: [{
        type: Schema.Types.ObjectId,
        ref: "User"
    }],
    owner: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
}, {
    timestamps: true
});

RestaurantSchema.static("createRestaurant", async(data: IRestaurant) => {
    const { name } = data;

    // verify: other restaurant don't have the same name
    const sameNameRestaurant = await Restaurant.findOne({ name });
    if (sameNameRestaurant) {
        throw Error("Restaurant name already have been taken.");
    }

    return Restaurant.create(data);
});

RestaurantSchema.static("updateRestaurant", async(data: IRestaurant) => {
    const { id, name } = data;

    // verify: other restaurant don't have the same name
    const sameNameRestaurant = await Restaurant.findOne({
        _id: { $ne: new ObjectId(id) },
        name
    });
    if (sameNameRestaurant) {
        throw Error("Restaurant name already have been taken.");
    }

    await Restaurant.updateOne({
        _id: new ObjectId(id)
    }, data);
});

RestaurantSchema.static("fakeData", () => ({
    name: faker.company.companyName(),
    description: faker.lorem.sentence(20),
    image: `${faker.image.business(640, 320)}/${faker.datatype.number(999)}`
} as IRestaurant));

RestaurantSchema.virtual("mealsCount", {
    ref: "Meal",
    localField: "meals",
    foreignField: "_id",
    count: true
});

RestaurantSchema.pre("deleteOne", {
    document: true,
    query: false
}, async function(this: IRestaurant, next) {
    await Meal.deleteMany({
        restaurant: this._id
    });
    await Order.deleteMany({
        restaurant: this._id
    });
    next();
});

// Fix for hot-reloading
const Restaurant: RestaurantModel = mongoose.models?.Restaurant as RestaurantModel || mongoose.model<IRestaurant, RestaurantModel>("Restaurant", RestaurantSchema);

export default Restaurant;
