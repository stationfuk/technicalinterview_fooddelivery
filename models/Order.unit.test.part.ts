import Meal from "./Meal";
import Order from "./Order";
import Restaurant from "./Restaurant";
import User, { UserType } from "./User";

function createOwner() {
    return User.createUser({
        name: "Test owner 1",
        email: "test_owner_1@example.org",
        password: "secret_password",
        type: UserType.OWNER
    });
}
function createClient() {
    return User.createUser({
        name: "Test client 1",
        email: "test_client_1@example.org",
        password: "secret_password",
        type: UserType.CLIENT
    });
}

export default () => describe("Order", () => {
    it("multiple can be created", async() => {
        const owner = await createOwner();
        const client = await createClient();
        await expect(Order.create({
            history: [{
                status: "PLACED",
                time: Date.now()
            }],
            meals: [
                new Meal(Meal.fakeData()),
                new Meal(Meal.fakeData()),
                new Meal(Meal.fakeData())
            ],
            restaurant: await Restaurant.createRestaurant({
                ...Restaurant.fakeData(),
                owner
            }),
            client
        })).resolves.not.toThrow();
        await expect(Order.create({
            history: [{
                status: "PLACED",
                time: Date.now() - 1000
            }, {
                status: "CANCELED",
                time: Date.now()
            }],
            meals: [
                new Meal(Meal.fakeData()),
                new Meal(Meal.fakeData()),
                new Meal(Meal.fakeData())
            ],
            restaurant: await Restaurant.createRestaurant({
                ...Restaurant.fakeData(),
                owner
            }),
            client
        })).resolves.not.toThrow();
    });
});
