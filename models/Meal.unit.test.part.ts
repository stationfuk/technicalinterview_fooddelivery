import Meal from "./Meal";

export default () => describe("Meal", () => {
    it("multiple can be created", async() => {
        await expect(Meal.createMeal(Meal.fakeData())).resolves.not.toThrow();
        await expect(Meal.createMeal(Meal.fakeData())).resolves.not.toThrow();
    });
});
