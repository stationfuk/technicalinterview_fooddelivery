import faker from "faker";
import mongoose, { Document, Model, Schema } from "mongoose";
import { IRestaurant } from "./Restaurant";
import ObjectId = mongoose.Types.ObjectId;

export interface IMeal extends Document {
    name: string;
    description: string;
    price: number;
    image: string;

    restaurant: string | ObjectId | IRestaurant;
}

interface MealModel extends Model<IMeal> {
    createMeal(data: {
        name: string;
        description: string;
        price: number;
        image: string;
        restaurant: string | ObjectId | IRestaurant;
    }): Promise<IMeal>;
    fakeData(): IMeal;
}

const MealSchema = new Schema<IMeal, MealModel>({
    name: {
        type: String,
        required: [true, "Please provide a name."],
        maxlength: [50, "Name cannot be longer than 50 characters."]
    },
    description: {
        type: String,
        required: [true, "Please provide a description."],
        maxlength: [250, "Description cannot be longer than 250 characters."]
    },
    price: {
        type: Number,
        required: [true, "Please provide a price."],
        min: [0.01, "Price cannot be lower than $0.01."]
    },
    image: {
        type: String
    },
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: "Restaurant"
    }
}, {
    timestamps: true
});

MealSchema.static("createMeal", async(data: IMeal) => {
    const restaurant = new Meal(data);
    await restaurant.save();
    return restaurant;
});

MealSchema.static("fakeData", () => ({
    name: faker.company.companyName(),
    description: faker.lorem.sentence(5),
    price: faker.datatype.number({
        min: 1,
        max: 20,
        precision: 2
    }),
    image: faker.image.food()
} as IMeal));

// Fix for hot-reloading
const Meal: MealModel = mongoose.models?.Meal as MealModel || mongoose.model<IMeal, MealModel>("Meal", MealSchema);

export default Meal;
