import faker from "faker";
import mongoose, { Document, Model, Schema } from "mongoose";
import crypto from "crypto";

export enum UserType {
    CLIENT = "CLIENT",
    OWNER = "OWNER"
}

export type CreateUserParams = {
    name: string;
    email: string;
    password: string;
    type: string;
};

export interface IUser extends Document {
    name: string;
    email: string;
    passwordHash: string;
    passwordSalt: string;
    type: UserType;

    validatePassword(password: string): boolean;
}

interface UserModel extends Model<IUser> {
    hashPassword(password: string, salt?: string): {
        passwordHash: string;
        passwordSalt: string;
    };
    createUser(data: CreateUserParams): Promise<IUser>;
    fakeData(): IUser;
}

const UserSchema = new Schema<IUser, UserModel>({
    name: {
        type: String,
        required: [true, "Please provide a name."],
        maxlength: [20, "Name cannot be longer than 20 characters."]
    },
    email: {
        type: String,
        required: [true, "Please provide an email."],
        unique: true
    },
    passwordHash: { type: String },
    passwordSalt: { type: String },
    type: { type: String }
}, {
    timestamps: true
});

UserSchema.method("validatePassword", function(this: IUser, password: string) {
    return this.passwordHash === User.hashPassword(password, this.passwordSalt).passwordHash;
});

UserSchema.static("hashPassword", (password: string, salt?: string) => {
    let localSalt = salt;
    if (!localSalt) {
        localSalt = crypto.randomBytes(16).toString("hex");
    }
    const passwordHash = crypto
        .pbkdf2Sync(password, localSalt, 1000, 64, "sha512")
        .toString("hex");
    return {
        passwordHash,
        passwordSalt: localSalt
    };
});

UserSchema.static("createUser", async(data: CreateUserParams) => {
    const { name, email, password, type } = data;

    const existingUser = await User.findOne({ email });
    if (existingUser) {
        throw new Error("Email already have been taken.");
    }

    const { passwordSalt, passwordHash } = User.hashPassword(password);

    const user = new User({
        name,
        email,
        passwordHash,
        passwordSalt,
        type
    });

    await user.save();

    return user;
});

UserSchema.static("fakeData", () => new User({
    name: faker.name.firstName(),
    email: faker.internet.email(),
    type: faker.random.arrayElement([
        UserType.CLIENT,
        UserType.OWNER
    ]),
    // "qwe"
    passwordHash: "3fafecddc21d5d42df89d09b4a45c55f226e151e7e88af0ed87dec2570a05c73d564cb5bb76a41f4c484fcaa9968204314f27dce0b91ed56785c1d1739c3dab4",
    passwordSalt: "f19e722c5111033695163324447373d4"
}));

// Fix for hot-reloading
const User: UserModel = mongoose.models?.User as UserModel || mongoose.model<IUser, UserModel>("User", UserSchema);

export default User;
