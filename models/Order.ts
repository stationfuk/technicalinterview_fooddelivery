import mongoose, { Document, Model, Schema, Types } from "mongoose";
import ObjectId = mongoose.Types.ObjectId;
import { IRestaurant } from "./Restaurant";
import { IUser } from "./User";

export enum OrderStatus {
    "PLACED" = "PLACED",
    "CANCELED" = "CANCELED",
    "PROCESSING" = "PROCESSING",
    "IN_ROUTE" = "IN_ROUTE",
    "DELIVERED" = "DELIVERED",
    "RECEIVED" = "RECEIVED"
}

export type OrderMeal = {
    name: string;
    price: number;
    amount: number;
};

export type OrderHistoryEntry = {
    status: OrderStatus;
    time: number;
};

export interface IOrder extends Document {
    // copy of meals data, not using IMeal as we don't want
    // to change existing order data when change meal data
    meals: OrderMeal[];
    history: OrderHistoryEntry[];

    client: string | ObjectId | IUser;
    restaurant: string | ObjectId | IRestaurant;
}

type OrderModel = Model<IOrder>;

const OrderSchema = new Schema<IOrder, OrderModel>({
    meals: { type: [Object] },
    history: { type: [Object] },
    client: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: "Restaurant"
    }
}, {
    timestamps: true
});

// Fix for hot-reloading
const Order: OrderModel = mongoose.models?.Order as OrderModel || mongoose.model<IOrder, OrderModel>("Order", OrderSchema);

export default Order;
