import Restaurant from "./Restaurant";
import User, { UserType } from "./User";

function createOwner() {
    return User.createUser({
        name: "Test owner 1",
        email: "test@example.org",
        password: "secret_password",
        type: UserType.OWNER
    });
}

export default () => describe("Restaurant", () => {
    it("multiple can be created", async() => {
        const owner = await createOwner();
        await expect(Restaurant.createRestaurant({
            ...Restaurant.fakeData(),
            owner
        })).resolves.not.toThrow();
        await expect(Restaurant.createRestaurant({
            ...Restaurant.fakeData(),
            owner
        })).resolves.not.toThrow();
    });
    it("cannot be created with same name", async() => {
        const owner = await createOwner();
        const restaurant1 = Restaurant.fakeData();
        const restaurant2 = Restaurant.fakeData();
        restaurant2.name = restaurant1.name;
        await expect(Restaurant.createRestaurant({
            ...restaurant1,
            owner
        })).resolves.not.toThrow();
        await expect(Restaurant.createRestaurant({
            ...restaurant2,
            owner
        })).rejects.toThrow();
    });
});
