+1      Users
+1.1    User can create account or login into existing
+1.2    Support role Client
+       * see all restaurants (except blocked)
+       * place orders in restaurant
+       * see list of orders
+1.2    Support role Owner
+       * CRUD restaurants and meals
+       * block Client in specific restaurant
+       * see list of orders


+2      Restaurant Database Model
+2.1    Have fields: name, description (of the type of food they serve)
+2.2    Have list field: blocked users


+3      Meal Database Model
+3.1    Have fields: name, description, and price


+4      Order Database Model
+4.1    Have fields: date, total amount, status, restaurant
+4.2    Have list field: list of meals (copy data, not refs)
+4.2    Have list field: history of date and time of the status changing


+5      Order workflow
+5.1    Restaurant Owners and Regular Users can change the Order Status respecting below flow and permissions:
+       * Placed: Once a Regular user places an Order
+       * Canceled: If the Regular User cancel the Order
+       * Processing: Once the Restaurant Owner starts to make the meals
+       * In Route: Once the meal is finished and Restaurant Owner marks its on the way
+       * Delivered: Once the Restaurant Owner receives information that the meal was delivered by their staff
+       * Received: Once the Regular User receives the meal and marks it as Received
+5.2    Status of order can be changed only by specified user
+5.3    Status of order can be changed upwards


+6      System Design
+6.1    REST/GraphQL API
+6.2    Perform all user actions via the API (including authentication)
+6.3    App should be SPA
+6.4    All actions done by AJAX
+6.5    Functional UI/UX design (not unique, use best practices)


+7       Tests
+7.1     Functional tests that use the REST/GraphQL Layer directly (via Postman)
+7.2     (optional) Unit tests
-7.3     (optional) E2E tests

+8       Interview preparation
+8.1     Explain how a REST/GraphQL API works
+8.2     Make requests to API works use REST/GraphQL clients like Postman, cURL
