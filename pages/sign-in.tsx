import React from "react";
import AuthForm from "../components/AuthForm";
import { useUser } from "../lib/hooks/useUser";

export default function SignIn() {
    useUser({
        redirectTo: "/",
        redirectIfFound: true
    });

    return (
        <AuthForm
            isSignIn={true}
        />
    );
}
