import "../styles/globals.scss";
import type { AppProps } from "next/app";
import AppFooter from "../components/AppFooter";
import AppHeader from "../components/AppHeader";
import dynamic from "next/dynamic";

dynamic(() => import("jquery"), { ssr: false });
dynamic(() => import("bootstrap"), { ssr: false });

function App({ Component, pageProps }: AppProps) {
    return (
        <>
            <AppHeader />

            <main>
                <Component {...pageProps} />
            </main>

            <AppFooter />
        </>
    );
}
export default App;
