import Head from "next/head";
import React, { useState } from "react";
import OrdersList from "../components/OrdersList";
import Helpers from "../lib/Helpers";
import { useOrders } from "../lib/hooks/useOrders";
import { useUser } from "../lib/hooks/useUser";
import Link from "next/link";
import { OrderStatus } from "../models/Order";

export default function Orders() {
    useUser({
        redirectTo: "/sign-in"
    });

    const { orders, mutateOrders } = useOrders();

    const [errorMessage, setErrorMessage] = useState("");
    const [isActiveRequest, setIsActiveRequest] = useState(false);

    async function changeStatus(orderId: string, status: OrderStatus) {
        setErrorMessage("");
        setIsActiveRequest(true);
        try {
            const [response] = await Promise.all([
                fetch(`/api/order/${orderId}/change-status`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        status
                    })
                }),
                Helpers.wait(1500)
            ]);
            if (response.status === 200) {
                await mutateOrders();
                setIsActiveRequest(false);
            } else {
                throw Error(await response.text());
            }
        } catch (error) {
            console.error(error);
            setErrorMessage(error.data?.message || error.message);
            setIsActiveRequest(false);
        }
    }

    return (
        <div className="container py-5">

            <Head>
                <title>Food Delivery | Orders</title>
            </Head>

            <div className="row mb-3 g-0">
                <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link href="/" passHref={true}>
                                    <a>Home</a>
                                </Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                Orders
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div className="alert alert-danger" role="alert" hidden={!errorMessage}>
                {errorMessage}
            </div>
            <h2 className="mb-3">
                Orders
            </h2>
            <OrdersList
                orders={orders}
                changeStatus={changeStatus}
                actionBtnDisabled={isActiveRequest}
                openFirstByDefault={false}
            />
        </div>
    );
}

// to force SSR, not static generation
export async function getServerSideProps() {
    return { props: { } };
}
