import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import React from "react";
import styles from "../styles/Home.module.scss";
import cx from "classnames";

const Home: NextPage = () => (
    <div className="container py-5">
        <Head>
            <title>
                Food Delivery | Home
            </title>
            <link rel="icon" href="/favicon.ico" />
        </Head>

        <div className="row mb-5">
            <div className="col-12">
                <h1 className={cx(styles.title, "display-6")}>
                    Welcome to Food Delivery!
                </h1>
            </div>
        </div>

        <div className="row justify-content-center">
            <div className="col-sm-12 col-md-6 col-lg-4">
                <Link href="/restaurants" passHref={true}>
                    <a className={cx(styles.card, "d-flex flex-column")}>
                        <h2>Restaurants &rarr;</h2>
                        <p>Explore the best restaurants and make orders</p>
                    </a>
                </Link>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-4 d-flex">
                <Link href="/orders" passHref={true}>
                    <a className={cx(styles.card, "d-flex flex-column")}>
                        <h2>Orders &rarr;</h2>
                        <p>View list of your orders and check their statuses</p>
                    </a>
                </Link>
            </div>
        </div>
    </div>
);

export default Home;
