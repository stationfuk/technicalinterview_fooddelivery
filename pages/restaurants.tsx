import Head from "next/head";
import { useRouter } from "next/router";
import React, { useState } from "react";
import AddCard from "../components/AddCard";
import Pagination from "../components/Pagination";
import RestaurantCard from "../components/RestaurantCard";
import RestaurantEditBlockedUsersModalForm from "../components/RestaurantEditBlockedUsersModalForm";
import { useRestaurants } from "../lib/hooks/useRestaurants";
import { FetchUser, useUser } from "../lib/hooks/useUser";
import Restaurant, { IRestaurant } from "../models/Restaurant";
import { UserType } from "../models/User";
import Link from "next/link";
import RestaurantEditModalForm from "../components/RestaurantEditModalForm";
import { SWRConfig } from "swr";

const POSSIBLE_PAGE_SIZE = [24, 48, 96];

function Restaurants() {
    const { user } = useUser({
        redirectTo: "/sign-in"
    });
    const router = useRouter();

    const [page, setPage] = useState((router.query.page || 1) as number);
    const [pageSize, setPageSize] = useState((router.query.pageSize || POSSIBLE_PAGE_SIZE[0]) as number);

    const [editingRestaurant, setEditingRestaurant] = useState(null as IRestaurant);
    const [editingBlockedUsers, setEditingBlockedUsers] = useState(null as IRestaurant);

    const { restaurants, restaurantsTotalCount, mutateRestaurants } = useRestaurants({
        page,
        pageSize
    });

    if (!restaurants) {
        return null;
    }

    const totalPages = Math.ceil(restaurantsTotalCount / pageSize) || 1;

    // for case when we delete only entry on last page
    // there will be no entries on page, so switch to new last page
    if (page > totalPages) {
        setPage(totalPages);
    }

    const restaurantsList = restaurants.map((restaurant, i) =>
        <RestaurantCard
            key={i}
            restaurant={restaurant}
            handleEdit={() => editRestaurant(restaurant)}
            handleEditBlockedUsers={() => editBlockedUsers(restaurant)}
            handleDelete={() => deleteRestaurant(restaurant)}
        />
    );

    function editRestaurant(restaurant?: IRestaurant) {
        setEditingRestaurant(restaurant || new Restaurant());
    }

    function editBlockedUsers(restaurant: IRestaurant) {
        setEditingBlockedUsers(restaurant);
    }

    async function deleteRestaurant(restaurant: IRestaurant) {
        await fetch(`/api/restaurant/${restaurant.id}`, {
            method: "DELETE"
        });
        await mutateRestaurants();
    }

    function handlePageChange(pageNum: number) {
        setPage(pageNum);
    }

    const restaurantsLabel = user?.type === UserType.OWNER
        ? "My Restaurants"
        : "Restaurants";

    return (
        <div className="container py-5">

            <Head>
                <title>Food Delivery | {restaurantsLabel}</title>
            </Head>

            <div className="row mb-3 g-0">
                <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link href="/" passHref={true}>
                                    <a>Home</a>
                                </Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                {restaurantsLabel}
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <h2 className="mb-3">
                {restaurantsLabel}
            </h2>
            <div className="row">
                {restaurantsList}
                {
                    user?.type === UserType.OWNER &&
                    <AddCard title="restaurant" handleClick={editRestaurant} />
                }
            </div>
            <Pagination
                totalPages={totalPages}
                activePage={page}
                handleSelect={handlePageChange}
            />
            {
                editingRestaurant &&
                <RestaurantEditModalForm
                    restaurant={editingRestaurant}
                    mutateRestaurants={mutateRestaurants}
                    close={() => setEditingRestaurant(null)}
                />
            }
            {
                editingBlockedUsers &&
                <RestaurantEditBlockedUsersModalForm
                    restaurantId={editingBlockedUsers.id}
                    close={() => setEditingBlockedUsers(null)}
                />
            }
        </div>
    );
}

export default function RestaurantsPage({ fallback }) {
    return (
        <SWRConfig value={{ fallback }}>
            <Restaurants />
        </SWRConfig>
    );
}

// to force SSR, not static generation
export async function getServerSideProps(context) {
    const user = await FetchUser.fetcher(context.req.headers.cookie);
    return {
        props: {
            fallback: {
                [FetchUser.URL]: user
            }
        }
    };
}
