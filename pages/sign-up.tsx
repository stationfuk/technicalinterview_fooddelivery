import React from "react";
import AuthForm from "../components/AuthForm";
import { useUser } from "../lib/hooks/useUser";

export default function SignUp() {
    useUser({
        redirectTo: "/",
        redirectIfFound: true
    });

    return (
        <AuthForm
            isSignIn={false}
        />
    );
}
