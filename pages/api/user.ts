import { nextConnectWithSession } from "../../lib/nextConnectWithSession";
import User from "../../models/User";
import { Types } from "mongoose";

export default nextConnectWithSession()
    .get(async(req, res) => {
        const user = req.session.get("user");
        if (!user) {
            res.json({
                isLoggedIn: false
            });
            return;
        }

        const fullUser = await User.findOne({
            _id: new Types.ObjectId(user.id)
        });
        if (!fullUser) {
            req.session.destroy();
            res.json({
                isLoggedIn: false
            });
            return;
        }

        res.json({
            name: fullUser.name,
            email: fullUser.email,
            type: fullUser.type,
            isLoggedIn: true
        });
    });
