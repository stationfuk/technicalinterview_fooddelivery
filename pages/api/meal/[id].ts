import formidable from "formidable";
import { Types } from "mongoose";
import ensureUserIsOwner from "../../../lib/auth/ensureUserIsOwner";
import Helpers from "../../../lib/Helpers";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../../lib/nextConnectWithSession";
import parseRequestForm from "../../../lib/parseRequestForm";
import Meal from "../../../models/Meal";
import Restaurant, { IRestaurant } from "../../../models/Restaurant";
import fs from "fs-extra";

export const config = {
    api: {
        // using multipart form for uploading image
        bodyParser: false
    }
};

export default nextConnectWithSession()
    .put(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is owner-typed user
            const user = ensureUserIsOwner(req, res);
            if (!user) {
                return;
            }

            // parse form data
            let {
                fields: {
                    name,
                    description,
                    price,
                    image,
                    restaurantId
                },
                files
            } = await parseRequestForm(req, [
                "name",
                "description",
                "price",
                "image",
                "restaurantId"
            ], [
                "image"
            ]);

            // if new image was uploaded - save it to disk
            if (files.image) {
                // verify: image size < 500 kB
                const imageFile = files.image as formidable.File;
                if (imageFile.size > 500 * 1024) {
                    throw Error("Image size cannot exceed 500 kB.");
                }

                // save to disk
                const imagePath = `/images/meals/${Helpers.generateUUIDv4()}`;
                await fs.ensureDir("./public/images/meals/");
                await fs.move(
                    imageFile.path,
                    `./public${imagePath}`
                );
                image = imagePath;
            }

            const id = req.query.id?.toString();

            const meal = id !== "new" && await Meal
                .findOne({
                    _id: new Types.ObjectId(id)
                }).populate("restaurant");

            if (!meal) {
                // verify: only owner of restaurant can add meals
                const restaurant = await Restaurant
                    .findOne({
                        _id: new Types.ObjectId(restaurantId)
                    });

                if (!restaurant) {
                    res.status(404).end("Restaurant not found");
                    return;
                }

                if ((restaurant as IRestaurant).owner.toString() !== user.id) {
                    res.status(403).end("Access Denied");
                    return;
                }

                await Meal.createMeal({
                    name,
                    description,
                    image,
                    price: parseFloat(price),
                    restaurant: restaurantId
                });
            } else {
                // verify: only owner of restaurant can edit meals
                if ((meal.restaurant as IRestaurant).owner.toString() !== user.id) {
                    res.status(403).end("Access Denied");
                    return;
                }

                await meal.updateOne({
                    name,
                    description,
                    image,
                    price: parseFloat(price)
                });
            }

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    })
    .delete(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is owner-typed user
            const user = ensureUserIsOwner(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id?.toString();

            const meal = await Meal
                .findOne({
                    _id: new Types.ObjectId(id)
                }).populate("restaurant");

            if (!meal) {
                res.status(404).end();
                return;
            }

            if ((meal.restaurant as IRestaurant).owner.toString() !== user.id) {
                res.status(403).end("Access Denied");
                return;
            }

            await meal.deleteOne();

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });


