import { Types } from "mongoose";
import ensureUserIsClient from "../../../lib/auth/ensureUserIsClient";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../../lib/nextConnectWithSession";
import Meal from "../../../models/Meal";
import Order, { OrderStatus } from "../../../models/Order";
import Restaurant from "../../../models/Restaurant";

export default nextConnectWithSession()
    .put(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is client-typed user
            const user = ensureUserIsClient(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id?.toString();
            if (id !== "new") {
                throw Error("Not supported");
            }

            const { restaurantId, meals } = req.body;

            const restaurant = await Restaurant
                .findOne({
                    _id: new Types.ObjectId(restaurantId)
                });

            // verify: restaurant exists
            if (!restaurant) {
                res.status(404).end("Restaurant not found.");
                return;
            }

            // verify: client not blocked by restaurant owner
            if (restaurant.blockedUsers.indexOf(new Types.ObjectId(user.id)) > -1) {
                res.status(403).end("You have been blocked by restaurant owner.");
                return;
            }

            for (const mealData of meals) {
                const meal = await Meal
                    .findOne({
                        _id: new Types.ObjectId(mealData.id)
                    });

                // verify: meal exists
                if (!meal) {
                    res.status(404).end("Meal not found.");
                    return;
                }

                // verify: right meal price
                if (Math.abs(meal.price - mealData.price) > 0.001) {
                    res.status(500).end("Wrong meal price, reload page.");
                    return;
                }

                delete mealData.id;
            }

            await Order.create({
                history: [{
                    status: OrderStatus.PLACED,
                    time: Date.now()
                }],
                restaurant: new Types.ObjectId(restaurantId),
                meals,
                client: new Types.ObjectId(user.id)
            });

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });


