import { Types } from "mongoose";
import ensureUser from "../../../../lib/auth/ensureUser";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../../../lib/nextConnectWithSession";
import Order, { OrderStatus } from "../../../../models/Order";
import { IRestaurant } from "../../../../models/Restaurant";

export default nextConnectWithSession()
    .post(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is user
            const user = ensureUser(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id?.toString();

            const { status } = req.body;

            const order = await Order
                .findOne({
                    _id: new Types.ObjectId(id)
                })
                .populate("restaurant");

            // verify: restaurant exists
            if (!order) {
                res.status(404).end("Order not found.");
                return;
            }

            const isUserOrderClient = order.client.toString() === user.id;
            const isUserOrderRestaurantOwner = (order.restaurant as IRestaurant).owner.toString() === user.id;

            const lastHistoryEntry = order.history[order.history.length - 1];

            let wrongUser = false;
            let wrongStatusSequence = false;
            switch (status) {
                case OrderStatus.PLACED:
                    // status cannot be place via POST
                    wrongStatusSequence = true;
                    break;
                case OrderStatus.CANCELED:
                    wrongStatusSequence = lastHistoryEntry.status !== OrderStatus.PLACED;
                    wrongUser = !isUserOrderClient;
                    break;
                case OrderStatus.PROCESSING:
                    wrongStatusSequence = lastHistoryEntry.status !== OrderStatus.PLACED;
                    wrongUser = !isUserOrderRestaurantOwner;
                    break;
                case OrderStatus.IN_ROUTE:
                    wrongStatusSequence = lastHistoryEntry.status !== OrderStatus.PROCESSING;
                    wrongUser = !isUserOrderRestaurantOwner;
                    break;
                case OrderStatus.DELIVERED:
                    wrongStatusSequence = lastHistoryEntry.status !== OrderStatus.IN_ROUTE;
                    wrongUser = !isUserOrderRestaurantOwner;
                    break;
                case OrderStatus.RECEIVED:
                    wrongStatusSequence = lastHistoryEntry.status !== OrderStatus.DELIVERED;
                    wrongUser = !isUserOrderClient;
                    break;
                default:
                    // status cannot be place via POST
                    wrongStatusSequence = true;
                    break;
            }

            if (wrongStatusSequence) {
                throw Error("Order status sequence mismatched.");
            }
            if (wrongUser) {
                res.status(403).end("Access Denied");
                return;
            }

            await order.update({
                history: [
                    ...order.history,
                    {
                        status,
                        time: Date.now()
                    }
                ]
            });

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });


