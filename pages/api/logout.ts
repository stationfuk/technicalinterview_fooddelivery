import { nextConnectWithSession } from "../../lib/nextConnectWithSession";

export default nextConnectWithSession()
    .post(async(req, res) => {
        req.session.destroy();
        res.json({
            isLoggedIn: false
        });
    });
