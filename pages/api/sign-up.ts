import { nextConnectWithSession } from "../../lib/nextConnectWithSession";
import MongooseDatabase from "../../lib/MongooseDatabase";
import User from "../../models/User";

export default nextConnectWithSession()
    .post(async(req, res) => {
        try {
            await MongooseDatabase.connect();

            const user = await User.createUser(req.body);

            req.session.set("user", {
                id: user.id,
                type: user.type
            });
            await req.session.save();

            res.json({
                email: user.email,
                name: user.name,
                type: user.type,
                isLoggedIn: true
            });
        } catch (error) {
            console.error(error);
            res.status(500).json({
                message: error.message
            });
        }
    });
