import * as faker from "faker";
import { nextConnectWithSession } from "../../../lib/nextConnectWithSession";
import Meal from "../../../models/Meal";
import Order, { OrderStatus } from "../../../models/Order";
import Restaurant from "../../../models/Restaurant";
import User, { UserType } from "../../../models/User";

export default nextConnectWithSession()
    .get(async(req, res) => {
        if (req.query.secret.toString() !== process.env.DATABASE_POPULATE_SECRET) {
            res.status(404).end();
            return;
        }

        req.session.destroy();

        await Restaurant.deleteMany({});
        await Meal.deleteMany({});
        await Order.deleteMany({});
        await User.deleteMany();

        const userOwner = await User.create({
            name: "Mike owner",
            email: "owner@example.org",
            passwordHash: "3fafecddc21d5d42df89d09b4a45c55f226e151e7e88af0ed87dec2570a05c73d564cb5bb76a41f4c484fcaa9968204314f27dce0b91ed56785c1d1739c3dab4",
            passwordSalt: "f19e722c5111033695163324447373d4",
            type: UserType.OWNER
        });

        const userOwner2 = await User.create({
            name: "Mike owner 2",
            email: "owner2@example.org",
            passwordHash: "3fafecddc21d5d42df89d09b4a45c55f226e151e7e88af0ed87dec2570a05c73d564cb5bb76a41f4c484fcaa9968204314f27dce0b91ed56785c1d1739c3dab4",
            passwordSalt: "f19e722c5111033695163324447373d4",
            type: UserType.OWNER
        });

        const userClient = await User.create({
            name: "Mike client",
            email: "client@example.org",
            passwordHash: "3fafecddc21d5d42df89d09b4a45c55f226e151e7e88af0ed87dec2570a05c73d564cb5bb76a41f4c484fcaa9968204314f27dce0b91ed56785c1d1739c3dab4",
            passwordSalt: "f19e722c5111033695163324447373d4",
            type: UserType.CLIENT
        });

        const userBlocked = await User.create({
            name: "Mike client blocked",
            email: "client_blocked@example.org",
            passwordHash: "3fafecddc21d5d42df89d09b4a45c55f226e151e7e88af0ed87dec2570a05c73d564cb5bb76a41f4c484fcaa9968204314f27dce0b91ed56785c1d1739c3dab4",
            passwordSalt: "f19e722c5111033695163324447373d4",
            type: UserType.CLIENT
        });

        // create many random users with password "qwe"
        await Promise.all(
            Array(50)
                .fill(null)
                .map(() => User.create(User.fakeData()))
        );

        const restaurant = await Restaurant.create({
            name: "McDonald's",
            description: "McDonald's is an American fast food company, founded in 1940 as a restaurant operated by Richard and Maurice McDonald, in San Bernardino",
            image: "/images/samples/mcdonalds/mcdonalds.png",
            owner: userOwner,
            blockedUsers: [
                userBlocked
            ]
        });

        const mcd1 = await Meal.create({
            name: "Big Mac",
            description: "Mouthwatering perfection starts with two 100% pure beef patties and Big Mac sauce sandwiched between a sesame seed bun.",
            price: 3.2,
            image: "/images/samples/mcdonalds/bigmac.jpg",
            restaurant
        });
        const mcd2 = await Meal.create({
            name: "Double Quarter Pounder with Cheese",
            description: "Each Double Quarter Pounder with Cheese features two quarter pound* 100% fresh beef burger patties that are hot, deliciously juicy and cooked when you order.",
            price: 4.3,
            image: "/images/samples/mcdonalds/double-quarter-pounder.jpg",
            restaurant
        });
        const mcd3 = await Meal.create({
            name: "World Famous Fries",
            description: "McDonald's World Famous Fries are made with premium potatoes such as the Russet Burbank and the Shepody",
            price: 1.7,
            image: "/images/samples/mcdonalds/fries.jpg",
            restaurant
        });

        await Order.create({
            meals: [{
                name: mcd1.name,
                amount: 3,
                price: mcd1.price
            }, {
                name: mcd2.name,
                amount: 1,
                price: mcd2.price
            }],
            history: [{
                status: OrderStatus.PLACED,
                time: Date.now() - faker.datatype.number(1000000)
            }],
            client: userClient,
            restaurant
        });

        await Order.create({
            meals: [{
                name: mcd3.name,
                amount: 5,
                price: mcd3.price
            }],
            history: [{
                status: OrderStatus.PLACED,
                time: Date.now() - 3 * 60 * 1000
            }, {
                status: OrderStatus.CANCELED,
                time: Date.now() - 2 * 60 * 1000
            }],
            client: userClient,
            restaurant
        });

        res.end("Success!");
    });
