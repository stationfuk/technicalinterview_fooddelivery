import ensureUser from "../../lib/auth/ensureUser";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../lib/nextConnectWithSession";
import Order, { IOrder } from "../../models/Order";
import Restaurant, { IRestaurant } from "../../models/Restaurant";
import { IUser, UserType } from "../../models/User";
import { Types } from "mongoose";

export default nextConnectWithSession()
    .get(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            const user = ensureUser(req, res);
            if (!user) {
                return;
            }

            let orders: IOrder[];

            if (user.type === UserType.CLIENT) {
                orders = await Order
                    .find({
                        client: new Types.ObjectId(user.id)
                    })
                    .sort({
                        createdAt: -1
                    })
                    .populate("restaurant")
                    .populate("client");
            } else {
                const restaurants = await Restaurant
                    .find({
                        owner: new Types.ObjectId(user.id)
                    });
                orders = await Order
                    .find({
                        restaurant: {
                            $in: restaurants
                        }
                    })
                    .sort({
                        createdAt: -1
                    })
                    .populate("restaurant")
                    .populate("client");
            }

            res.status(200).json({
                orders: orders.map((order) => ({
                    id: order.id,
                    meals: order.meals,
                    history: order.history,
                    restaurant: {
                        id: (order.restaurant as IRestaurant).id,
                        name: (order.restaurant as IRestaurant).name
                    },
                    client: {
                        id: (order.client as IUser).id,
                        name: (order.client as IUser).name
                    }
                }))
            });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });
