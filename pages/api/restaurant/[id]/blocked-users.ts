import { Types } from "mongoose";
import ensureUserIsOwner from "../../../../lib/auth/ensureUserIsOwner";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../../../lib/nextConnectWithSession";
import Restaurant, { IRestaurant } from "../../../../models/Restaurant";
import User, { IUser, UserType } from "../../../../models/User";

export default nextConnectWithSession()
    .get(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            const user = ensureUserIsOwner(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id.toString();
            const restaurant = await Restaurant
                .findOne({
                    _id: new Types.ObjectId(id)
                }).populate("blockedUsers");

            if (!restaurant) {
                res.status(404).end("Restaurant not found");
                return;
            }

            if ((restaurant as IRestaurant).owner.toString() !== user.id) {
                res.status(403).end("Access Denied");
                return;
            }

            const { blockedUsers } = restaurant;
            const notBlockedUsers = await User
                .find({
                    _id: {
                        $nin: blockedUsers
                    },
                    type: UserType.CLIENT
                })
                .sort({
                    createdAt: -1
                });

            res.status(200).json({
                blockedUsers: blockedUsers.map((blockedUser: IUser) => ({
                    id: blockedUser.id,
                    name: blockedUser.name
                })),
                notBlockedUsers: notBlockedUsers.map((notBlockedUser: IUser) => ({
                    id: notBlockedUser.id,
                    name: notBlockedUser.name
                }))
            });
        } catch (e) {
            console.log(e);
            throw e;
        }
    })
    .put(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is owner-typed user
            const user = ensureUserIsOwner(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id?.toString();
            const blockedUsersIds = req.body.blockedUsers
                .map((blockedUser) => new Types.ObjectId(blockedUser.id));

            const restaurant = await Restaurant
                .findOne({
                    _id: new Types.ObjectId(id)
                });

            if (!restaurant) {
                res.status(404).end("Restaurant not found");
                return;
            }

            // verify: only owner can edit restaurant
            if (restaurant.owner.toString() !== user.id) {
                res.status(403).end("Access Denied");
                return;
            }

            // verify: users are
            // 1) exists in database
            // 2) client-typed
            const blockedUsers = await User.find({
                _id: {
                    $in: blockedUsersIds
                },
                type: UserType.CLIENT
            });

            await restaurant.updateOne({
                blockedUsers
            });

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });
