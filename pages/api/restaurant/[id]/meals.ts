import { Types } from "mongoose";
import ensureUser from "../../../../lib/auth/ensureUser";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../../../lib/nextConnectWithSession";
import Meal, { IMeal } from "../../../../models/Meal";

export default nextConnectWithSession()
    .get(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            const user = ensureUser(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id.toString();
            const page = parseInt(req.query.page.toString(), 10);
            const pageSize = parseInt(req.query.pageSize.toString(), 10);

            const filter = {
                restaurant: new Types.ObjectId(id)
            };

            const meals = await Meal
                .find(filter)
                .sort({
                    createdAt: -1
                })
                .skip(pageSize * (page - 1))
                .limit(pageSize);
            const mealsTotalCount = await Meal.countDocuments(filter);

            res.status(200).json({
                mealsTotalCount,
                meals: meals.map((meal: IMeal) => ({
                    id: meal.id,
                    name: meal.name,
                    description: meal.description,
                    price: meal.price,
                    image: meal.image
                }))
            });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });
