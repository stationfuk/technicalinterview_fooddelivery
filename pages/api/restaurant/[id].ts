import formidable from "formidable";
import { Types } from "mongoose";
import ensureUser from "../../../lib/auth/ensureUser";
import ensureUserIsOwner from "../../../lib/auth/ensureUserIsOwner";
import Helpers from "../../../lib/Helpers";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../../lib/nextConnectWithSession";
import parseRequestForm from "../../../lib/parseRequestForm";
import Restaurant from "../../../models/Restaurant";
import { UserType } from "../../../models/User";
import fs from "fs-extra";

export const config = {
    api: {
        // using multipart form for uploading image
        bodyParser: false
    }
};

export default nextConnectWithSession()
    .get(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            const user = ensureUser(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id.toString();
            const restaurant = await Restaurant
                .findOne({
                    _id: new Types.ObjectId(id)
                });

            // verify: restaurant exists
            if (!restaurant) {
                res.status(404).end("Restaurant not found.");
                return;
            }

            // verify: client-typed user no blocked by owner
            if (user.type === UserType.CLIENT && restaurant.blockedUsers.indexOf(new Types.ObjectId(user.id)) > -1) {
                res.status(403).end("You have been blocked by restaurant owner.");
                return;
            }

            res.status(200).json({
                id: restaurant.id,
                name: restaurant.name,
                description: restaurant.description,
                image: restaurant.image
            });
        } catch (e) {
            console.log(e);
            throw e;
        }
    })
    .put(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is owner-typed user
            const user = ensureUserIsOwner(req, res);
            if (!user) {
                return;
            }

            // parse form data
            let {
                fields: {
                    name,
                    description,
                    image
                },
                files
            } = await parseRequestForm(req, [
                "name",
                "description",
                "image"
            ], [
                "image"
            ]);

            // if new image was uploaded - save it to disk
            if (files.image) {
                // verify: image size < 500 kB
                const imageFile = files.image as formidable.File;
                if (imageFile.size > 500 * 1024) {
                    throw Error("Image size cannot exceed 500 kB.");
                }

                // save to disk
                const imagePath = `/images/restaurants/${Helpers.generateUUIDv4()}`;
                await fs.ensureDir("./public/images/restaurants/");
                await fs.move(
                    imageFile.path,
                    `./public${imagePath}`
                );
                image = imagePath;
            }
            const id = req.query.id?.toString();

            const restaurant = id !== "new" && await Restaurant
                .findOne({
                    _id: new Types.ObjectId(id)
                });

            if (!restaurant) {
                await Restaurant.createRestaurant({
                    name,
                    description,
                    image,
                    owner: user.id
                });
            } else {
                // verify: only owner can edit restaurant
                if (restaurant.owner.toString() !== user.id) {
                    res.status(403)
                        .end("Access Denied");
                    return;
                }
                await Restaurant.updateRestaurant({
                    id,
                    name,
                    description,
                    image
                });
            }

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    })
    .delete(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            // verify: is owner-typed user
            const user = ensureUserIsOwner(req, res);
            if (!user) {
                return;
            }

            const id = req.query.id?.toString();

            const restaurant = await Restaurant
                .findOne({
                    _id: new Types.ObjectId(id)
                });

            if (!restaurant) {
                res.status(404).end("Restaurant not found");
                return;
            }

            if (restaurant.owner.toString() !== user.id) {
                res.status(403).end("Access Denied");
                return;
            }

            // cascade delete of meals and orders
            // via .pre("delete") in model
            await restaurant.deleteOne();

            res.status(200)
                .json({
                    done: true
                });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });


