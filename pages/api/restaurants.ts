import ensureUser from "../../lib/auth/ensureUser";
import { nextConnectWithSession, NextIronRequest, NextIronResponse } from "../../lib/nextConnectWithSession";
import Restaurant from "../../models/Restaurant";
import { UserType } from "../../models/User";

export default nextConnectWithSession()
    .get(async(req: NextIronRequest, res: NextIronResponse) => {
        try {
            const user = ensureUser(req, res);
            if (!user) {
                return;
            }

            const filter = {
                blockedUsers: {
                    $nin: [user.id]
                },
                ...(user.type === UserType.OWNER ? {
                    owner: user.id
                } : {})
            };

            const pageSize = parseInt(req.query.pageSize.toString(), 10);
            const page = parseInt(req.query.page?.toString(), 10);

            const restaurants = await Restaurant.find(filter)
                .skip(pageSize * (page - 1))
                .limit(pageSize);
            const restaurantsTotalCount = await Restaurant.countDocuments(filter);

            res.status(200).json({
                restaurantsTotalCount,
                restaurants: restaurants.map((restaurant) => ({
                    id: restaurant.id,
                    name: restaurant.name,
                    description: restaurant.description,
                    image: restaurant.image
                }))
            });
        } catch (e) {
            console.log(e);
            throw e;
        }
    });
