import { nextConnectWithSession } from "../../lib/nextConnectWithSession";
import User from "../../models/User";

export default nextConnectWithSession()
    .post(async(req, res) => {
        try {
            const user = await User.findOne({
                email: req.body.email
            });
            if (!user || !user.validatePassword(req.body.password)) {
                res.status(403).json({
                    message: "Invalid username and password combination"
                });
                return;
            }

            req.session.set("user", {
                id: user.id,
                type: user.type
            });
            await req.session.save();

            res.json({
                email: user.email,
                name: user.name,
                type: user.type,
                isLoggedIn: true
            });
        } catch (error) {
            console.error(error);
            res.status(500).json({
                message: error.message
            });
        }
    });
