import Head from "next/head";
import React from "react";
import { useUser } from "../../lib/hooks/useUser";
import { useRouter } from "next/router";
import { useRestaurant } from "../../lib/hooks/useRestaurant";
import Image from "next/image";
import Link from "next/link";
import MealCardsList from "../../components/MealCardsList";
import { UserType } from "../../models/User";

export default function Restaurant() {
    const { user } = useUser({
        redirectTo: "/sign-in"
    });
    const router = useRouter();

    const { restaurant } = useRestaurant(router.query.id.toString());

    if (!restaurant) {
        return null;
    }

    const restaurantsLabel = user?.type === UserType.OWNER
        ? "My Restaurants"
        : "Restaurants";

    return (
        <div className="container py-5">

            <Head>
                <title>Food Delivery | {restaurant.name}</title>
            </Head>

            <div className="row mb-3 g-0">
                <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link href="/" passHref={true}>
                                    <a>Home</a>
                                </Link>
                            </li>
                            <li className="breadcrumb-item">
                                <Link href="/restaurants" passHref={true}>
                                    <a>{restaurantsLabel}</a>
                                </Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                {restaurant.name}
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div className="row mb-5">
                <div className="col-sm-12 col-md-3">
                    <div data-aspect-ratio="4:3">
                        <Image
                            src={restaurant.image}
                            alt={restaurant.name}
                            layout="fill"
                        />
                    </div>
                </div>
                <div className="col-sm-12 col-md-9">
                    <h1 className="mb-1 display-6">
                        {restaurant.name}
                    </h1>
                    <p className="lead">
                        {restaurant.description}
                    </p>
                </div>
            </div>
            <MealCardsList
                restaurant={restaurant}
            />
        </div>
    );
}

// to force SSR, not static generation
export async function getServerSideProps() {
    return { props: { } };
}
