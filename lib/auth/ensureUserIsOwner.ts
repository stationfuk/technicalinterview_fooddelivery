import { UserType } from "../../models/User";
import { NextIronRequest, NextIronResponse } from "../nextConnectWithSession";
import ensureUser from "./ensureUser";

export default function ensureUserIsOwner(req: NextIronRequest, res: NextIronResponse) {
    const user = ensureUser(req, res);
    if (!user || user.type !== UserType.OWNER) {
        res.status(403).end("Access Denied");
        return null;
    }
    return user;
}
