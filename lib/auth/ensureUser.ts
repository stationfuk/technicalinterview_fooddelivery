import { NextIronRequest, NextIronResponse } from "../nextConnectWithSession";

export default function ensureUser(req: NextIronRequest, res: NextIronResponse) {
    const user = req.session.get("user");
    if (!user) {
        res.status(403).end("Access Denied");
        return null;
    }
    return user;
}
