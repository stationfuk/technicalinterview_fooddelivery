import mongoose from "mongoose";
import Logger from "./Logger";

/**
 * Global is used here to maintain a cached connection across hot reloads
 * in development. This prevents connections growing exponentiatlly
 * during API Route usage.
 */

const { MONGODB_URI } = process.env;
if (!MONGODB_URI) {
    throw new Error(
        "Please define the MONGODB_URI environment variable inside .env.local"
    );
}

class MongooseDatabase {

  private static connecting = false;

  private static cache: {
    conn?: typeof mongoose;
    promise?: Promise<void | typeof mongoose>;
  } = {};

  static async connect() {
      if (this.cache.conn) {
          return;
      }

      this.connecting = true;

      if (!this.cache.promise) {
          const opts = {
              keepAlive: true,
              useNewUrlParser: true,
              useUnifiedTopology: true,
              useFindAndModify: false,
              useCreateIndex: true
          };

          this.cache.promise = mongoose.connect(MONGODB_URI, opts)
              .then((mongooseConn) => {
                  this.cache.conn = mongooseConn;
              });

          // If the connection throws an error
          mongoose.connection.on("error", (err) => {
              Logger.error("Mongoose default connection error", err);
              throw err;
          });
      }
      await this.cache.promise;

      this.connecting = false;
  }

  static async disconnect() {
      if (this.cache.conn) {
          await this.cache.conn.disconnect();
      }
  }

}

export default MongooseDatabase;
