import formidable, { IncomingForm } from "formidable";
import { NextIronRequest } from "./nextConnectWithSession";

export default async function parseRequestForm(
    req: NextIronRequest,
    fieldNames: string[],
    filesNames: string[]
) {
    return new Promise<{
        fields: {
            [key: string]: string;
        };
        files: {
            [key: string]: formidable.File;
        };
    }>((resolve, reject) => {
        const form = new IncomingForm();
        form.parse(req, (err, fields, files) => {
            if (err) {
                reject(err);
                return;
            }
            const fieldsObj = {};
            for (const fieldName of fieldNames) {
                fieldsObj[fieldName] = fields[fieldName]?.toString();
            }
            const filesObj = {};
            for (const filesName of filesNames) {
                filesObj[filesName] = files[filesName];
            }
            resolve({
                fields: fieldsObj,
                files: filesObj
            });
        });
    });
}
