export class IDictionary<T> implements Iterable<T> {

    [index: string]: T

    constructor(objArr?: {
        [index: string]: T
    }) {
        if (objArr) {
            for (const objKey in objArr) {
                this[objKey] = objArr[objKey];
            }
        }
    }

    *[Symbol.iterator](): Iterator<T> {
        const keys = Object.values(this);
        for (const i of keys) {
            yield i;
        }
    }

}
