import chalk from "chalk";
import moment from "moment";
import { serializeError } from "serialize-error";

type MsgTypeColor = {
    text: [number, number, number];
    bg: [number, number, number];
};

class Logger {

    static isNode = typeof process !== "undefined";

    static shouldCollectLogs = false;

    static collectedLogs: string[] = [];

    private static colors = {
        info: {
            text: [255, 255, 255],
            bg: [163, 204, 245]
        } as MsgTypeColor,
        warn: {
            text: [102, 77, 3],
            bg: [255, 243, 205]
        } as MsgTypeColor,
        error: {
            text: [35, 55, 82],
            bg: [226, 202, 205]
        } as MsgTypeColor,
        time: {
            text: [0, 0, 0],
            bg: [230, 230, 230]
        } as MsgTypeColor
    };

    private static getTime() {
        return moment().format("DD.MM HH:mm:ss.SSS");
    }

    private static log({ args, addPredNewline, addPostNewline }: {
        args: any[];
        addPredNewline: boolean;
        addPostNewline: boolean;
    }) {
        // 1. Serialize all objects to strings
        for (let i = 0; i < args.length; i++) {
            if (args[i] instanceof Error) {
                args[i] = serializeError(args[i]);
            }
            if (typeof args[i] === "object") {
                args[i] = JSON.stringify(args[i], null, 2);
            }
        }

        // 2. (optional) Save log message without formatting
        if (this.shouldCollectLogs) {
            this.collectedLogs.push(`${String(addPredNewline ? "\n" : "") +
            args.join(" ")}\n${
                addPostNewline ? "\n" : ""}`);
        }

        let msgTypeColor: MsgTypeColor | undefined;
        switch (args[0]) {
            case "INFO ":
                msgTypeColor = this.colors.info;
                break;
            case "WARN ":
                msgTypeColor = this.colors.warn;
                break;
            case "ERROR":
                msgTypeColor = this.colors.error;
                break;
        }
        if (msgTypeColor) {
            if (this.isNode) {
                args[0] = chalk
                    .bgRgb(...msgTypeColor.bg)
                    .rgb(...msgTypeColor.text)(` ${args[0]}`);
                // For time
                args[1] = chalk
                    .bgRgb(...this.colors.time.bg)
                    .rgb(...this.colors.time.text)(args[1]);
            } else {
                args[0] = `%c ${args[0]}%c %c ${args[1]} `;
                args.splice(1, 1, [
                    `font-weight: 600; background: rgb(${msgTypeColor.bg.join(",")}); color: rgb(${msgTypeColor.text.join(",")});`,
                    "",
                    "background: rgb(230, 230, 230);"
                ]);
            }
        }
        if (addPredNewline) {
            args[0] = `\n${args[0]}`;
        }
        if (addPostNewline) {
            args[args.length - 1] = `${args[args.length - 1]}\n`;
        }
        // eslint-disable-next-line no-console
        console.log(...args);
    }

    static info(...args: any[]) {
        const newlines = this.extractPredPostNewlines(args);
        args.unshift(this.getTime());
        args.unshift("INFO ");
        this.log({
            args,
            ...newlines
        });
    }

    static warning(...args: any[]) {
        const newlines = this.extractPredPostNewlines(args);
        args.unshift(this.getTime());
        args.unshift("WARN ");
        this.log({
            args,
            ...newlines
        });
    }

    static error(...args: any[]) {
        const newlines = this.extractPredPostNewlines(args);
        args.unshift(this.getTime());
        args.unshift("ERROR");
        this.log({
            args,
            ...newlines
        });
    }

    private static extractPredPostNewlines(args: any[]) {
        const addPredNewline = args.length > 0 && args[0] === "\n";
        if (addPredNewline) {
            args.shift();
        }
        const addPostNewline = args.length > 0 && args[args.length - 1] === "\n";
        if (addPostNewline) {
            args.pop();
        }
        return {
            addPredNewline,
            addPostNewline
        };
    }

    static borderedMessage(msg: string, color?: string) {
        const msgStrings = msg.split("\n");
        const maxMsgStringLength = Math.max(...msgStrings.map((str) => str.length));
        if (typeof window === "undefined") {
            const coloredChalk = chalk.bgRgb(163, 204, 145);
            console.log(
                `${`${coloredChalk(Array(maxMsgStringLength + 6).fill("*")
                    .join(""))}\n` +
                `${coloredChalk("*")}${Array(maxMsgStringLength + 4).fill(" ")
                    .join("")}${coloredChalk("*")}\n`}${
                    msgStrings.map((str) => `${coloredChalk("*")}  ${str}${new Array(maxMsgStringLength - str.length).fill(" ")
                        .join("")}  ${coloredChalk("*")}\n`).join("")
                }${coloredChalk("*")}${Array(maxMsgStringLength + 4).fill(" ")
                    .join("")}${coloredChalk("*")}\n` +
                `${coloredChalk(Array(maxMsgStringLength + 6).fill("*")
                    .join(""))}`
            );
        } else {
            const borderStyle = `font-size: 16px; font-weight: 900; background-color: ${color};`;
            const msgStyle = "font-size: 16px; font-weight: 900;";
            console.log(
                `${`%c${Array(maxMsgStringLength + 6).fill("*")
                    .join("")}\n` +
                `*%c${Array(maxMsgStringLength + 4).fill(" ")
                    .join("")}%c*\n`}${
                    msgStrings.map((str) => `*%c  ${str}${new Array(maxMsgStringLength - str.length).fill(" ")
                        .join("")}  %c*\n`).join("")
                }*%c${Array(maxMsgStringLength + 4).fill(" ")
                    .join("")}%c*\n` +
                `${Array(maxMsgStringLength + 6).fill("*")
                    .join("")}`,
                borderStyle,
                msgStyle,
                borderStyle,
                ...Array(msgStrings.length).fill([
                    msgStyle,
                    borderStyle
                ])
                    .flat(),
                msgStyle,
                borderStyle
            );
        }
    }

    static CreateSeparateLogger(shouldCollectLogs = false) {
        return class extends Logger {

            static shouldCollectLogs = shouldCollectLogs;

            static collectedLogs: string[] = [];

        };
    }

}

export default Logger;
