// this file is a wrapper with defaults to be used in both API routes and `getServerSideProps` functions
import { NextApiRequest, NextApiResponse } from "next";
import nextConnect, { NextConnect } from "next-connect";
import { ironSession, Session, SessionOptions } from "next-iron-session";
import MongooseDatabase from "./MongooseDatabase";

// add stronger typing for next-specific implementation
export type NextIronRequest = NextApiRequest & { session: Session };
export type NextIronResponse = NextApiResponse;

async function ensureMongoDbConnected(req, res, next) {
    await MongooseDatabase.connect();
    next();
}

export const nextConnectWithSession = (props?: {
    ensureUser?: boolean;
    ensureUserOfType?: boolean;
}) => nextConnect()
    .use(ironSession({
        password: process.env.TOKEN_SECRET,
        cookieName: process.env.COOKIE_NAME,
        cookieOptions: {
            // allows to use the session in non-https environments like
            // Next.js dev mode (http://localhost:3000)
            secure: process.env.NODE_ENV === "production"
        }
    } as SessionOptions))
    .use(ensureMongoDbConnected) as NextConnect<NextIronRequest, NextIronResponse>;
