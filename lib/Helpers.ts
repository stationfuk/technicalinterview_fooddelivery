import { v4 as uuidv4 } from "uuid";

export default class Helpers {

    static generateUUIDv4() {
        return uuidv4().toString();
    }

    static wait(minTime: number, maxTime?: number) {
        return new Promise((resolve) => {
            setTimeout(resolve, Helpers.randomIntFromInterval(minTime, maxTime || minTime));
        });
    }

    static randomIntFromInterval(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

}
