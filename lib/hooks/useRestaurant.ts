import Router from "next/router";
import { useEffect } from "react";
import useSWR from "swr";
import { IRestaurant } from "../../models/Restaurant";

const fetchRestaurant = (url, restaurantId) =>
    fetch(`${url}/${restaurantId}`)
        .then(async(r) => {
            if (r.status !== 200) {
                throw await r.text() || r.statusText;
            }
            return r;
        })
        .then((r) => r.json());

export function useRestaurant(restaurantId: string) {
    const {
        data: restaurant,
        mutate: mutateRestaurant,
        error
    } = useSWR<IRestaurant>([
        "/api/restaurant",
        restaurantId
    ], fetchRestaurant);

    useEffect(() => {
        if (error) {
            console.error(error);
            Router.push("/restaurants");
        }
    }, [error]);

    return {
        restaurant: error
            ? null
            : restaurant,
        mutateRestaurant
    };
}
