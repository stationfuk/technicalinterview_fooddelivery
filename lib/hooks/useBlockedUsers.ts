import useSWR from "swr";

const fetchBlockedUsers = (url) =>
    fetch(`${url}`)
        .then((r) => r.json());

export function useBlockedUsers(restaurantId: string) {
    const {
        data,
        mutate: mutateBlockedUsers,
        error
    } = useSWR<{
        blockedUsers: {
            id: string;
            name: string;
        }[];
        notBlockedUsers: {
            id: string;
            name: string;
        }[];
    }>([
        `/api/restaurant/${restaurantId}/blocked-users`
    ], fetchBlockedUsers);

    const blockedUsers = error
        ? null
        : data?.blockedUsers || null;

    const notBlockedUsers = error
        ? null
        : data?.notBlockedUsers || null;

    return {
        blockedUsers,
        notBlockedUsers,
        mutateBlockedUsers
    };
}
