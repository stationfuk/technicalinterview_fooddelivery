import useSWR from "swr";
import { IRestaurant } from "../../models/Restaurant";

const fetchWithPageAndPageSize = (url, page, pageSize) =>
    fetch(`${url}?page=${page}&pageSize=${pageSize}`)
        .then((r) => r.json());

type Params = {
    page?: number;
    pageSize?: number;
};

export function useRestaurants({ page, pageSize }: Params) {
    const {
        data,
        error,
        mutate: mutateRestaurants
    } = useSWR<{
        restaurantsTotalCount: number;
        restaurants: IRestaurant[]
    }>([
        "/api/restaurants",
        page,
        pageSize
    ], fetchWithPageAndPageSize);

    const restaurants = !error
        ? data?.restaurants || null
        : null;
    const restaurantsTotalCount = data?.restaurantsTotalCount || 0;

    return {
        restaurantsTotalCount,
        restaurants,
        mutateRestaurants
    };
}
