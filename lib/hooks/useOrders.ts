import useSWR from "swr";
import { IOrder } from "../../models/Order";

const fetchOrders = (url) =>
    fetch(url)
        .then((r) => r.json());

export function useOrders() {
    const {
        data,
        error,
        mutate: mutateOrders
    } = useSWR<{
        orders: IOrder[]
    }>([
        "/api/orders"
    ], fetchOrders);

    const orders = !error
        ? data?.orders || null
        : null;

    return {
        orders,
        mutateOrders
    };
}
