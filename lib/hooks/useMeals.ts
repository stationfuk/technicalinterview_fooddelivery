import useSWR from "swr";
import { IMeal } from "../../models/Meal";

const fetchWithPageAndPageSize = (url, page, pageSize) =>
    fetch(`${url}?page=${page}&pageSize=${pageSize}`)
        .then((r) => r.json());

type Params = {
    restaurantId?: string;
    page?: number;
    pageSize?: number;
};

export function useMeals({ restaurantId, page, pageSize }: Params) {
    const {
        data,
        error,
        mutate: mutateMeals
    } = useSWR<{
        mealsTotalCount: number;
        meals: IMeal[]
    }>([
        `/api/restaurant/${restaurantId}/meals`,
        page,
        pageSize
    ], fetchWithPageAndPageSize);

    const meals = !error
        ? data?.meals || null
        : null;
    const mealsTotalCount = data?.mealsTotalCount || 0;

    return {
        mealsTotalCount,
        meals,
        mutateMeals
    };
}
