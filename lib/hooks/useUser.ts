import { useEffect } from "react";
import Router from "next/router";
import useSWR from "swr";
import { UserType } from "../../models/User";

export namespace FetchUser {
    export const URL = `${process.env.BASE_URI || ""}/api/user`;
    export type TResponse = {
        name: string;
        email: string;
        type: UserType;
        isLoggedIn: boolean;
    };
    export function fetcher(authCookie?: string): Promise<TResponse> {
        return fetch(URL, {
            ...(authCookie ? {
                headers: {
                    Cookie: authCookie
                }
            } : {})
        }).then((r) => r.json());
    }
}

type Params = {
    redirectTo?: string;
    redirectIfFound?: boolean;
};

export function useUser({ redirectTo, redirectIfFound }: Params = {}) {
    const {
        data: user,
        mutate: mutateUser,
        error
    } = useSWR<FetchUser.TResponse>(
        FetchUser.URL,
        FetchUser.fetcher
    );

    useEffect(() => {
        // if no redirect needed, just return (example: already on /dashboard)
        // if user data not yet there (fetch in progress, logged in or not) then don't do anything yet
        if (!redirectTo || !user) {
            return;
        }

        if (
            // If redirectTo is set, redirect if the user was not found.
            (redirectTo && !redirectIfFound && !user?.isLoggedIn) ||
            // If redirectIfFound is also set, redirect if the user was found
            (redirectIfFound && user?.isLoggedIn)
        ) {
            Router.push(redirectTo);
        }
    }, [user, redirectIfFound, redirectTo, error]);

    return {
        user,
        mutateUser
    };
}
