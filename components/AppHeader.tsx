import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import fetchJson from "../lib/fetchJson";
import { useUser } from "../lib/hooks/useUser";
import Link from "next/link";
import React from "react";
import cx from "classnames";

dynamic(() => import("bootstrap/js/dist/collapse"), { ssr: false });

export default function AppHeader() {
    const { user, mutateUser } = useUser();
    const router = useRouter();

    async function logout(e: React.FormEvent<HTMLAnchorElement>) {
        e.preventDefault();
        const loggedOutUser = await fetchJson("/api/logout", {
            method: "POST"
        });
        await Promise.all([
            mutateUser(loggedOutUser),
            router.push("/")
        ]);
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <div className="container">
                <Link href="/">
                    <a className="navbar-brand">
                        Food Delivery Service
                    </a>
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto">
                        {
                            user?.isLoggedIn &&
                            <>
                                <li className="nav-item">
                                    <Link href="/restaurants" passHref={true}>
                                        <a className={cx("nav-link", { active: router.pathname === "/restaurants" })}>
                                            Restaurants
                                        </a>
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link href="/orders" passHref={true}>
                                        <a className={cx("nav-link", { active: router.pathname === "/orders" })}>
                                            Orders
                                        </a>
                                    </Link>
                                </li>
                            </>
                        }
                    </ul>
                    {
                        user?.isLoggedIn &&
                        <span className="navbar-text mx-3">
                            Welcome, {user.name}!
                        </span>
                    }
                    <form className="d-flex">
                        {
                            user?.isLoggedIn &&
                            // eslint-disable-next-line @next/next/no-html-link-for-pages
                            <a href="/api/logout"
                                className="btn btn-outline-danger"
                                onClick={logout}
                            >
                                Logout
                            </a>
                        }
                        {
                            !user?.isLoggedIn &&
                            <>
                                <Link href="/sign-in" passHref={true}>
                                    <a className="btn btn-outline-primary" type="submit">
                                        Sign in
                                    </a>
                                </Link>
                                <Link href="/sign-up" passHref={true}>
                                    <a className="btn btn-outline-secondary ms-2" type="submit">
                                        Sign up
                                    </a>
                                </Link>
                            </>
                        }
                    </form>
                </div>
            </div>
        </nav>
    );
}

