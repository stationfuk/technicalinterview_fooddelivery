import React, { useState } from "react";
import { IOrder, OrderStatus } from "../models/Order";
import OrderListRow from "./OrderListRow";

type Props = {
    orders: IOrder[];
    openFirstByDefault: boolean;
    actionBtnDisabled: boolean;
    changeStatus(orderId: string, status: OrderStatus): void;
};

export default function OrdersList({ orders, openFirstByDefault, actionBtnDisabled, changeStatus }: Props) {
    const [openedOrderId, setOpenedOrderId] = useState(openFirstByDefault
        ? orders[0].id
        : null
    );

    if (!orders) {
        return null;
    }

    const ordersElements = orders.map((order, i) =>
        <OrderListRow
            key={i}
            order={order}
            isOpen={order.id === openedOrderId}
            actionBtnDisabled={actionBtnDisabled}
            open={() => setOpenedOrderId(order.id)}
            close={() => setOpenedOrderId(null)}
            changeStatus={(status) => changeStatus(order.id, status)} />
    );

    return (
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Datetime</th>
                    <th>Client</th>
                    <th>Restaurant</th>
                    <th>Total meals</th>
                    <th>Total cost</th>
                    <th>Order status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {ordersElements}
            </tbody>
        </table>
    );
}
