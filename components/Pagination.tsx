import cx from "classnames";
import React from "react";

type Props = {
    activePage: number;
    totalPages: number;
    handleSelect(page: number): void;
};

export default function Pagination({ activePage, totalPages, handleSelect }: Props) {
    if (totalPages < 2) {
        return null;
    }

    const prevActive = activePage !== 1;
    const nextActive = activePage < totalPages;

    function changePage(
        e: React.FormEvent<HTMLSpanElement>,
        pageNum: number,
        disabled?: boolean
    ) {
        if (!disabled) {
            handleSelect(pageNum);
        }
    }

    const pageNumElements = Array(totalPages).fill(0)
        .map((num, i) => (
            <li
                key={i}
                className={cx({ active: i + 1 === activePage }, "page-item")}
                aria-current="page">
                <span className="page-link cursor-pointer"
                    onClick={(e) => changePage(e, i + 1, i + 1 === activePage)}
                >
                    {i + 1}
                </span>
            </li>
        ));

    return (
        <div className="row py-2">
            <div className="col-12">
                <nav>
                    <ul className="pagination justify-content-center">
                        <li className={cx({ disabled: !prevActive }, "page-item")}>
                            <span
                                className="page-link cursor-pointer"
                                onClick={(e) => changePage(e, activePage - 1, !prevActive)}
                            >
                                Previous
                            </span>
                        </li>
                        {pageNumElements}
                        <li className={cx({ disabled: !nextActive }, "page-item")}>
                            <span
                                className="page-link cursor-pointer"
                                onClick={(e) => changePage(e, activePage + 1, !nextActive)}
                            >
                                Next
                            </span>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    );
}

