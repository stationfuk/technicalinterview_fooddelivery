export default function AppFooter() {
    return (
        <footer className="text-muted py-3 border-top">
            <div className="container d-flex justify-content-between">
                <span>&reg; {new Date().getFullYear()} Food Delivery, Inc.</span>
                <a className="float-end" href="#">Back to top</a>
            </div>
        </footer>
    );
}

