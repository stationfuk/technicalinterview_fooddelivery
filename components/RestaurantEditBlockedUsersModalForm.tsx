import React, { useEffect, useState } from "react";
import Helpers from "../lib/Helpers";
import { useBlockedUsers } from "../lib/hooks/useBlockedUsers";
import styles from "../styles/EditRestaurantForm.module.scss";
import ModalWrapper from "./ModalWrapper";
import UserSearchInput from "./UserSearchInput";

type Props = {
    restaurantId: string;
    close(): void;
};

export default function RestaurantEditBlockedUsersModalForm({
    restaurantId, close
}: Props) {
    const {
        blockedUsers: blockedUsersSWR,
        notBlockedUsers: notBlockedUsersSWR,
        mutateBlockedUsers
    } = useBlockedUsers(restaurantId);

    const [blockedUsers, setBlockedUsers] = useState(null as {
        id: string;
        name: string;
    }[]);
    const [notBlockedUsers, setNotBlockedUsers] = useState(null as {
        id: string;
        name: string;
    }[]);
    const [errorMessage, setErrorMessage] = useState("");
    const [isActiveRequest, setIsActiveRequest] = useState(false);

    useEffect(() => {
        if (blockedUsers === null && blockedUsersSWR) {
            setBlockedUsers(blockedUsersSWR);
        }
        if (notBlockedUsers === null && notBlockedUsersSWR) {
            setNotBlockedUsers(notBlockedUsersSWR);
        }
    }, [blockedUsers, notBlockedUsers, blockedUsersSWR, notBlockedUsersSWR]);

    if (!blockedUsers) {
        return null;
    }

    function addBlockedUser(userId: string) {
        const [user] = notBlockedUsers
            .filter((notBlockedUser) => notBlockedUser.id === userId);
        if (!user) {
            return;
        }
        const notBlockedUserIndex = notBlockedUsers.indexOf(user);
        const newNotBlockedUsers = [...notBlockedUsers];
        newNotBlockedUsers.splice(notBlockedUserIndex, 1);
        setBlockedUsers([
            user,
            ...blockedUsers
        ]);
        setNotBlockedUsers(newNotBlockedUsers);
    }

    function removeBlockedUser(
        e: React.FormEvent<HTMLButtonElement>,
        userId: string
    ) {
        e.preventDefault();
        e.stopPropagation();
        const [user] = blockedUsers
            .filter((blockedUser) => blockedUser.id === userId);
        if (!user) {
            return;
        }
        const blockedUserIndex = blockedUsers.indexOf(user);
        const newBlockedUsers = [...blockedUsers];
        newBlockedUsers.splice(blockedUserIndex, 1);
        setBlockedUsers(newBlockedUsers);
        setNotBlockedUsers([
            user,
            ...notBlockedUsers
        ]);
    }

    async function save() {
        setIsActiveRequest(true);
        try {
            const [response] = await Promise.all([
                fetch(`/api/restaurant/${restaurantId}/blocked-users`, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        blockedUsers
                    })
                }),
                Helpers.wait(1500)
            ]);
            if (response.status === 200) {
                await mutateBlockedUsers();
                await close();
            } else {
                throw Error(await response.text());
            }
        } catch (error) {
            console.error(error);
            setErrorMessage(error.data?.message || error.message);
            setIsActiveRequest(false);
        }
    }

    const blockedUserElems = blockedUsers.map((blockedUser, i) => (
        <li key={i} className="list-group-item d-flex justify-content-between align-items-center">
            <span>
                {blockedUser.name}
            </span>
            <button className="btn btn-danger" onClick={(e) => removeBlockedUser(e, blockedUser.id)}>
                Remove
            </button>
        </li>
    ));

    return (
        <ModalWrapper
            show={true}
            title="Edit blocked users"
            save={save}
            close={close}
            saveDisabled={isActiveRequest}
        >
            <form className={styles.editRestaurantForm}>

                <div className="alert alert-danger w-100" role="alert" hidden={!errorMessage}>
                    {errorMessage}
                </div>

                <b>Add blocked user</b>
                <UserSearchInput
                    users={notBlockedUsers}
                    select={addBlockedUser}
                />

                <b>Blocked users</b>
                <ul className="list-group mt-2">
                    {blockedUserElems}
                </ul>
            </form>
        </ModalWrapper>
    );
}

