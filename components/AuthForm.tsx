import cx from "classnames";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import fetchJson from "../lib/fetchJson";
import Helpers from "../lib/Helpers";
import { useUser } from "../lib/hooks/useUser";
import { UserType } from "../models/User";
import styles from "../styles/AuthForm.module.scss";

type Props = {
    isSignIn: boolean;
};

export default function AuthForm({ isSignIn }: Props) {
    const { mutateUser } = useUser();
    const router = useRouter();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [type, setType] = useState(UserType.CLIENT);
    const [isActiveRequest, setIsActiveRequest] = useState(false);

    const [errorMessage, setErrorMessage] = useState("");

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        if (errorMessage) {
            setErrorMessage("");
        }

        setIsActiveRequest(true);

        const body = {
            email,
            password,
            ...(!isSignIn ? {
                name,
                type
            } : {})
        };

        try {
            const endpointUrl = isSignIn
                ? "/api/sign-in"
                : "/api/sign-up";

            const [user] = await Promise.all([
                fetchJson(endpointUrl, {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(body)
                }),
                Helpers.wait(1500)
            ]);

            await Promise.all([
                mutateUser(user, false),
                router.push("/")
            ]);
        } catch (error) {
            console.error(error);
            setErrorMessage(error.data?.message || error.message);
            setIsActiveRequest(false);
        }
    }

    const actionLabel = isSignIn
        ? "Sign in"
        : "Sign up";

    return (
        <>
            <Head>
                <title>{actionLabel}</title>
            </Head>

            <div className="d-flex justify-content-center align-items-center text-center h-100">
                <form onSubmit={handleSubmit} className={cx(styles.authForm, "my-5", {
                    [styles.signIn]: isSignIn
                })}>
                    <h1 className="h3 mb-3 fw-normal">
                        {actionLabel}
                    </h1>

                    <div className="alert alert-danger text-start" role="alert" hidden={!errorMessage}>
                        {errorMessage}
                    </div>

                    <div className="form-floating" hidden={isSignIn}>
                        <input
                            type="text"
                            className={cx(styles.inputName, "form-control")}
                            id="input-name"
                            placeholder="John"
                            value={name}
                            onChange={(e) => setName(e.currentTarget.value)}
                        />
                        <label htmlFor="input-name">
                            Name
                        </label>
                    </div>

                    <div className="form-floating">
                        <input
                            type="email"
                            className={cx(styles.inputEmail, "form-control")}
                            id="input-email"
                            placeholder="name@example.com"
                            value={email}
                            onChange={(e) => setEmail(e.currentTarget.value)}
                        />
                        <label htmlFor="input-email">
                            Email
                        </label>
                    </div>

                    <div className="form-floating">
                        <input
                            type="password"
                            className={cx(styles.inputPassword, "form-control")}
                            id="input-password"
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.currentTarget.value)}
                        />
                        <label htmlFor="input-password">
                            Password
                        </label>
                    </div>

                    <div className={cx(styles.btnGroup, "btn-group mt-3 w-100")} hidden={isSignIn}>
                        <input
                            type="radio"
                            className="btn-check"
                            name="btnradio"
                            id="btnradio1"
                            autoComplete="off"
                            checked={type === UserType.CLIENT}
                            value={UserType.CLIENT}
                            onChange={(e) => setType(e.currentTarget.value as UserType)}
                        />
                        <label className="btn btn-outline-primary" htmlFor="btnradio1">
                            Restaurant client
                        </label>

                        <input
                            type="radio"
                            className="btn-check"
                            name="btnradio"
                            id="btnradio2"
                            autoComplete="off"
                            checked={type === UserType.OWNER}
                            value={UserType.OWNER}
                            onChange={(e) => setType(e.currentTarget.value as UserType)}
                        />
                        <label className="btn btn-outline-primary" htmlFor="btnradio2">
                            Restaurant owner
                        </label>
                    </div>

                    <button className="w-100 btn btn-lg btn-primary mt-3 mb-2" type="submit" disabled={isActiveRequest}>
                        {actionLabel}
                    </button>

                    {
                        isSignIn ? (
                            <Link href="/sign-up">
                                <a className="text-decoration-underline">
                                    I don&apos;t have an account
                                </a>
                            </Link>
                        ) : (
                            <Link href="/sign-in">
                                <a className="text-decoration-underline">
                                    I have an account
                                </a>
                            </Link>
                        )
                    }
                </form>
            </div>
        </>
    );
}
