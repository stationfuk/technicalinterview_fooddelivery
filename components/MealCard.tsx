import cx from "classnames";
import React, { useState } from "react";
import Image from "next/image";
import { useUser } from "../lib/hooks/useUser";
import { UserType } from "../models/User";
import styles from "../styles/MealCard.module.scss";
import { IMeal } from "../models/Meal";

type Props = {
    meal: IMeal;
    order: {
        amount: number;
        add(): void;
        remove(): void;
    }
    handleEdit(): void;
    handleDelete(): void;
};

export default function MealCard({ meal, order, handleEdit, handleDelete }: Props) {
    const { user } = useUser();

    const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);

    function handleEditLocal(e: React.FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        handleEdit();
    }

    function handleDeleteLocal(e: React.FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        if (showDeleteConfirm) {
            handleDelete();
            // to blur the button
            e.currentTarget.blur();
        } else {
            setShowDeleteConfirm(true);
        }
    }

    return (
        <div className={cx(styles.mealCard, "col-sm-12 col-md-6 col-lg-4 mb-3")}>
            <div className="card h-100">
                <div data-aspect-ratio="4:3">
                    <Image
                        src={meal.image}
                        alt={meal.name}
                        layout="fill"
                    />
                </div>
                <div className="card-body d-flex flex-column">
                    <h5 className="card-title">
                        {meal.name}
                    </h5>
                    <p className="card-text">
                        {meal.description}
                    </p>
                    <div className="card-text mt-auto">
                        <span className="lead">
                            <b>${meal.price.toFixed(2)}</b>
                        </span>
                        {
                            user?.type === UserType.CLIENT &&
                            <div className="float-end">
                                <button
                                    className="btn btn-outline-primary"
                                    onClick={order.remove}
                                    hidden={order.amount === 0}
                                >
                                    -
                                </button>
                                <span className="mx-3" hidden={order.amount === 0}>
                                    {order.amount}
                                </span>
                                <button
                                    className="btn btn-outline-primary"
                                    onClick={order.add}
                                >
                                    +
                                </button>
                            </div>
                        }
                    </div>
                </div>
                {
                    user?.type === UserType.OWNER &&
                    <div className={cx(styles.controlsBlock, "card-body border-top")}>
                        <div className="row">
                            <div className="col-12 mb-3">
                                <button className="btn btn-warning h-100 w-100" onClick={handleEditLocal}>
                                    Edit meal
                                </button>
                            </div>
                            <div className="col-12">
                                <button
                                    className="btn btn-danger h-100 w-100"
                                    onClick={handleDeleteLocal}
                                    onBlur={() => setShowDeleteConfirm(false)}
                                >
                                    {
                                        showDeleteConfirm
                                            ? <>
                                                <div>Meal will be permanently deleted!</div>
                                                <div>Click again to confirm</div>
                                            </>
                                            : "Delete"
                                    }
                                </button>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}

