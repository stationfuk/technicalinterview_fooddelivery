import cx from "classnames";
import React from "react";

type Props = {
    show: boolean;
    title: string;
    saveDisabled: boolean;
    save(): void;
    close(): void;
};

export default function ModalWrapper({ children, title, saveDisabled, show, save, close }: React.PropsWithChildren<Props>) {
    return (
        <>
            {
                show &&
                <div className="modal-backdrop fade show" />
            }
            <div className={cx({
                show,
                "d-block": show
            }, "modal")} tabIndex={-1} aria-hidden={!show} data-bs-backdrop="static" data-bs-keyboard="false">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                {title}
                            </h5>
                            <button type="button" className="btn-close" aria-label="Close" onClick={close} />
                        </div>
                        <div className="modal-body">
                            {children}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={close}>
                                Close
                            </button>
                            <button
                                type="button"
                                className="btn btn-primary"
                                onClick={save}
                                disabled={saveDisabled}
                            >
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

