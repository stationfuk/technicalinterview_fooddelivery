import cx from "classnames";
import moment from "moment";
import React from "react";
import { useUser } from "../lib/hooks/useUser";
import { IOrder, OrderStatus } from "../models/Order";
import { IRestaurant } from "../models/Restaurant";
import { IUser, UserType } from "../models/User";
import styles from "../styles/OrderListRow.module.scss";

type Props = {
    order: IOrder;
    isOpen: boolean;
    actionBtnDisabled: boolean;
    open(): void;
    close(): void;
    changeStatus(status: OrderStatus): void;
};

export default function OrderListRow({ order, isOpen, actionBtnDisabled, open, close, changeStatus }: Props) {
    const { user } = useUser();

    const lastHistoryEntry = order.history[order.history.length - 1];

    let actionBtnVisible = false;
    let actionBtnClass = "";
    let actionBtnLabel = "";
    let orderStatus = "";
    let nextStatus: OrderStatus;

    switch (lastHistoryEntry?.status) {
        case OrderStatus.PLACED:
            actionBtnVisible = true;
            actionBtnClass = user.type === UserType.CLIENT
                ? "btn-danger"
                : "btn-success";
            actionBtnLabel = user.type === UserType.CLIENT
                ? "Cancel order"
                : "Order cooking";
            orderStatus = "placed";
            nextStatus = user.type === UserType.CLIENT
                ? OrderStatus.CANCELED
                : OrderStatus.PROCESSING;
            break;
        case OrderStatus.CANCELED:
            actionBtnVisible = false;
            orderStatus = "canceled";
            break;
        case OrderStatus.PROCESSING:
            actionBtnVisible = user.type === UserType.OWNER;
            actionBtnClass = "btn-primary";
            actionBtnLabel = "Order on the way";
            orderStatus = "cooking";
            nextStatus = OrderStatus.IN_ROUTE;
            break;
        case OrderStatus.IN_ROUTE:
            actionBtnVisible = user.type === UserType.OWNER;
            actionBtnClass = "btn-primary";
            actionBtnLabel = "Order delivered";
            orderStatus = "on the way";
            nextStatus = OrderStatus.DELIVERED;
            break;
        case OrderStatus.DELIVERED:
            actionBtnVisible = user.type === UserType.CLIENT;
            actionBtnClass = "btn-success";
            actionBtnLabel = "I received order";
            orderStatus = "delivered";
            nextStatus = OrderStatus.RECEIVED;
            break;
        case OrderStatus.RECEIVED:
            actionBtnVisible = false;
            orderStatus = "received";
            break;
        default:
            actionBtnVisible = user.type === UserType.CLIENT;
            actionBtnClass = "btn-success";
            actionBtnLabel = "Place order";
            orderStatus = "not placed";
            break;
    }

    function handleActionBtnClick(e: React.FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        changeStatus(nextStatus);
    }

    const totalCost = order.meals
        .map((meal) => meal.price * meal.amount)
        .reduce((a, b) => a + b, 0);

    return (
        <>
            <tr
                className={cx(styles.orderListRow, {
                    [styles.open]: isOpen
                }, "cursor-pointer")}
                onClick={isOpen ? close : open}
            >
                <td>
                    {moment(lastHistoryEntry?.time || Date.now()).format("DD.MM.YY HH:mm")}
                </td>
                <td>
                    {(order.client as IUser).name}
                </td>
                <td>
                    {(order.restaurant as IRestaurant).name}
                </td>
                <td>
                    {order.meals.length}
                </td>
                <td>
                    ${totalCost.toFixed(2)}
                </td>
                <td>
                    {orderStatus}
                </td>
                <td>
                    <button
                        className={cx(actionBtnClass, "btn")}
                        onClick={handleActionBtnClick}
                        hidden={!actionBtnVisible}
                        disabled={actionBtnDisabled}
                    >
                        {actionBtnLabel}
                    </button>
                </td>
            </tr>
            {
                isOpen &&
                <tr
                    className={cx(styles.orderListRowDetails)}>
                    <td colSpan={7}>
                        <div className="row">
                            <div className="col-sm-12 col-md-6">
                                <table className="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Meal</th>
                                            <th>Amount</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            order.meals.map((meal, i) =>
                                                <tr key={i}>
                                                    <td>
                                                        {meal.name}
                                                    </td>
                                                    <td>
                                                        {meal.amount}
                                                    </td>
                                                    <td>
                                                        ${meal.price.toFixed(2)}
                                                    </td>
                                                    <td>
                                                        ${(meal.amount * meal.price).toFixed(2)}
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                            {
                                lastHistoryEntry &&
                                <div className="col-sm-12 col-md-6">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Date/time</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                order.history.map((historyEntry, i) =>
                                                    <tr key={i}>
                                                        <td>
                                                            {moment(historyEntry.time).format("DD.MM.YY HH:mm:ss")}
                                                        </td>
                                                        <td>
                                                            {historyEntry.status}
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            }
                        </div>
                    </td>
                </tr>
            }
        </>
    );
}

