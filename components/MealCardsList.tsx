import { useRouter } from "next/router";
import React, { useState } from "react";
import Helpers from "../lib/Helpers";
import { useMeals } from "../lib/hooks/useMeals";
import { useUser } from "../lib/hooks/useUser";
import Meal, { IMeal } from "../models/Meal";
import { IOrder } from "../models/Order";
import { IRestaurant } from "../models/Restaurant";
import { UserType } from "../models/User";
import AddCard from "./AddCard";
import MealCard from "./MealCard";
import MealEditModalForm from "./MealEditModalForm";
import OrdersList from "./OrdersList";
import Pagination from "./Pagination";

const POSSIBLE_PAGE_SIZE = [24, 48, 96];

type Props = {
    restaurant: IRestaurant;
};

export default function MealCardsList({ restaurant }: Props) {
    const { user } = useUser();
    const router = useRouter();

    const [page, setPage] = useState((router.query.page || 1) as number);
    const [pageSize, setPageSize] = useState((router.query.pageSize || POSSIBLE_PAGE_SIZE[0]) as number);

    const [editingMeal, setEditingMeal] = useState(null as IMeal);
    const [mealsToOrder, setMealsToOrder] = useState({} as {
        [key: string]: number;
    });

    const [errorMessage, setErrorMessage] = useState("");
    const [isSuccessPlaceOrderMessage, setIsSuccessPlaceOrderMessage] = useState(false);
    const [isActiveRequest, setIsActiveRequest] = useState(false);

    const { meals, mealsTotalCount, mutateMeals } = useMeals({
        restaurantId: restaurant.id,
        page,
        pageSize
    });

    if (!meals) {
        return null;
    }

    const totalPages = Math.ceil(mealsTotalCount / pageSize) || 1;

    // for case when we delete only entry on last page
    // there will be no entries on page, so switch to new last page
    if (page > totalPages) {
        setPage(totalPages);
    }

    function addToOrder(mealId: string) {
        setMealsToOrder((prevState) => {
            const immPrevState = { ...prevState };
            const amount = immPrevState[mealId] || 0;
            immPrevState[mealId] = amount + 1;
            return immPrevState;
        });
    }

    function removeFromOrder(mealId: string) {
        setMealsToOrder((prevState) => {
            const immPrevState = { ...prevState };
            const amount = immPrevState[mealId] || 0;
            immPrevState[mealId] = amount > 0
                ? amount - 1
                : 0;
            if (immPrevState[mealId] === 0) {
                delete immPrevState[mealId];
            }
            return immPrevState;
        });
    }

    async function placeOrder() {
        setErrorMessage("");
        setIsActiveRequest(true);
        try {
            const mealsList = Object.keys(mealsToOrder).map((mealId) => {
                const [meal] = meals.filter((mealEntry) => mealEntry.id === mealId);
                return {
                    id: mealId,
                    amount: mealsToOrder[mealId],
                    price: meal.price,
                    name: meal.name
                };
            });
            const [response] = await Promise.all([
                fetch("/api/order/new", {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        restaurantId: restaurant.id,
                        meals: mealsList
                    })
                }),
                Helpers.wait(1500)
            ]);
            if (response.status === 200) {
                setIsSuccessPlaceOrderMessage(true);
                setTimeout(() => {
                    setMealsToOrder({});
                    setIsSuccessPlaceOrderMessage(false);
                    setIsActiveRequest(false);
                    router.push("/orders");
                }, 2500);
            } else {
                throw Error(await response.text());
            }
        } catch (error) {
            console.error(error);
            setErrorMessage(error.data?.message || error.message);
            setIsActiveRequest(false);
        }
    }

    function editMeal(meal?: IMeal) {
        setEditingMeal(meal || new Meal());
    }

    async function deleteMeal(meal: IMeal) {
        await fetch(`/api/meal/${meal.id}`, {
            method: "DELETE"
        });
        await mutateMeals();
    }

    function handlePageChange(pageNum: number) {
        setPage(pageNum);
    }

    function combineMeals() {
        return Object.keys(mealsToOrder).map((mealId) => {
            const [meal] = meals.filter((mealEntry) => mealEntry.id === mealId);
            return {
                name: meal.name,
                amount: mealsToOrder[mealId],
                price: meal.price
            };
        });
    }

    const mealsList = (meals || []).map((meal, i) =>
        <MealCard
            key={i}
            meal={meal}
            order={{
                amount: mealsToOrder[meal.id] || 0,
                add: () => addToOrder(meal.id),
                remove: () => removeFromOrder(meal.id)
            }}
            handleEdit={() => editMeal(meal)}
            handleDelete={() => deleteMeal(meal)}
        />
    );

    return (
        <>
            <h2 className="mb-3">
                Menu
            </h2>
            <Pagination
                totalPages={totalPages}
                activePage={page}
                handleSelect={handlePageChange}
            />
            <div className="row">
                {mealsList}
                {
                    user?.type === UserType.OWNER &&
                    <AddCard title="meal" handleClick={editMeal} />
                }
            </div>
            {
                user.type === UserType.CLIENT && Object.keys(mealsToOrder).length > 0 &&
                <>
                    <h3>My order</h3>
                    <div className="alert alert-success" role="alert" hidden={!isSuccessPlaceOrderMessage}>
                        Order placed successfully!
                    </div>
                    <div className="alert alert-danger" role="alert" hidden={!errorMessage}>
                        {errorMessage}
                    </div>
                    <OrdersList
                        orders={[{
                            restaurant,
                            meals: combineMeals(),
                            client: {
                                name: user.name
                            },
                            history: []
                        } as IOrder]}
                        actionBtnDisabled={isActiveRequest}
                        openFirstByDefault={true}
                        changeStatus={() => placeOrder()}
                    />
                </>
            }
            {
                editingMeal &&
                <MealEditModalForm
                    meal={editingMeal}
                    mutateMeals={mutateMeals}
                    restaurantId={restaurant.id}
                    close={() => setEditingMeal(null)}
                />
            }
        </>
    );
}

