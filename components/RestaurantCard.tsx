import cx from "classnames";
import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import { useUser } from "../lib/hooks/useUser";
import { IRestaurant } from "../models/Restaurant";
import { UserType } from "../models/User";
import styles from "../styles/RestaurantCard.module.scss";

type Props = {
    restaurant: IRestaurant;
    handleEdit(): void;
    handleEditBlockedUsers(): void;
    handleDelete(): void;
};

export default function RestaurantCard({ restaurant, handleEdit, handleEditBlockedUsers, handleDelete }: Props) {
    const { user } = useUser();

    const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);

    function handleEditLocal(e: React.FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        handleEdit();
    }

    function handleEditBlockedUsersLocal(e: React.FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        handleEditBlockedUsers();
    }

    function handleDeleteLocal(e: React.FormEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        if (showDeleteConfirm) {
            handleDelete();
            // to blur the button
            e.currentTarget.blur();
        } else {
            setShowDeleteConfirm(true);
        }
    }

    return (
        <div className={cx(styles.restaurantCard, "col-sm-12 col-md-6 col-lg-4 mb-3")}>
            <Link href={`/restaurant/${restaurant.id}`} passHref={true}>
                <a className={cx("hoverable-card card h-100 cursor-pointer")}>
                    <div className="card-img-top">
                        <div data-aspect-ratio="4:3">
                            <Image
                                src={restaurant.image}
                                alt={restaurant.name}
                                layout="fill"
                            />
                        </div>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">
                            {restaurant.name}
                        </h5>
                        <p className="card-text">
                            {restaurant.description}
                        </p>
                    </div>
                    {
                        user?.type === UserType.OWNER &&
                        <div className={cx(styles.controlsBlock, "card-body border-top")}>
                            <div className="row">
                                <div className="col-sm-12 col-md-6 mb-3">
                                    <button className="btn btn-warning h-100 w-100" onClick={handleEditLocal}>
                                        Edit restaurant
                                    </button>
                                </div>
                                <div className="col-sm-12 col-md-6 mb-3">
                                    <button className="btn btn-warning h-100 w-100" onClick={handleEditBlockedUsersLocal}>
                                        Edit blocked users
                                    </button>
                                </div>
                                <div className="col-12">
                                    <button
                                        className="btn btn-danger h-100 w-100"
                                        onClick={handleDeleteLocal}
                                        onBlur={() => setShowDeleteConfirm(false)}
                                    >
                                        {
                                            showDeleteConfirm
                                                ? <>
                                                    <div>Restaurant will be permanently deleted!</div>
                                                    <div>Click again to confirm</div>
                                                </>
                                                : "Delete"
                                        }
                                    </button>
                                </div>
                            </div>
                        </div>
                    }
                </a>
            </Link>
        </div>
    );
}

