import cx from "classnames";
import React, { useState } from "react";
import Helpers from "../lib/Helpers";
import { IRestaurant } from "../models/Restaurant";
import styles from "../styles/EditRestaurantForm.module.scss";
import ModalWrapper from "./ModalWrapper";
import ImageUploading, { ImageListType } from "react-images-uploading";

type Props = {
    restaurant: IRestaurant;
    mutateRestaurants(): void;
    close(): void;
};

export default function RestaurantEditModalForm({ restaurant, mutateRestaurants, close }: Props) {
    const [name, setName] = useState(restaurant?.name || "");
    const [description, setDescription] = useState(restaurant?.description || "");
    const [image, setImage] = useState(restaurant?.image || "");
    const [errorMessage, setErrorMessage] = useState("");

    // const [uploadedImageFile, setUploadedImageFile] = useState(null as File);

    const [isActiveRequest, setIsActiveRequest] = useState(false);

    if (!restaurant) {
        return null;
    }

    function onImageChange(
        imageList: ImageListType
    ) {
        setErrorMessage("");
        if (imageList[0]) {
            const validateError = validateImage(imageList[0].file);
            if (validateError) {
                setErrorMessage(validateError);
                return;
            }
            // setUploadedImageFile(imageList[0].file);
            setImage(imageList[0].dataURL);
        }
    }

    function validateName() {
        if (name.length > 50) {
            return "Name length cannot exceed 50 symbols.";
        }
        return null;
    }

    function validateDescription() {
        if (description.length > 250) {
            return "Description length cannot exceed 250 symbols.";
        }
        return null;
    }

    function validateImage(file: File) {
        if (file.size > 500 * 1024) {
            return "Image size cannot exceed 500 kB.";
        }
        return null;
    }

    async function save() {
        let validateError = validateName();
        if (validateError) {
            setErrorMessage(validateError);
            return;
        }
        validateError = validateDescription();
        if (validateError) {
            setErrorMessage(validateError);
            return;
        }
        // validateError = uploadedImageFile && validateImage(uploadedImageFile);
        // if (validateError) {
        //     setErrorMessage(validateError);
        //     return;
        // }
        if (/* !uploadedImageFile && */!image) {
            setErrorMessage("Image cannot be empty.");
            return;
        }

        const formData = new FormData();
        formData.append("name", name);
        formData.append("description", description);
        formData.append(
            "image",
            /* uploadedImageFile || */image
        );

        setIsActiveRequest(true);
        try {
            const [response] = await Promise.all([
                fetch(`/api/restaurant/${restaurant.id || "new"}`, {
                    method: "PUT",
                    body: formData
                }),
                Helpers.wait(1500)
            ]);
            if (response.status === 200) {
                mutateRestaurants();
                close();
            } else {
                throw Error(await response.text());
            }
        } catch (error) {
            console.error(error);
            setErrorMessage(error.data?.message || error.message);
            setIsActiveRequest(false);
        }
    }

    return (
        <ModalWrapper
            show={true}
            title={`${restaurant.id ? "Edit" : "Add"} restaurant`}
            save={save}
            close={close}
            saveDisabled={isActiveRequest}
        >
            <form className={styles.editRestaurantForm}>

                <div className="alert alert-danger w-100" role="alert" hidden={!errorMessage}>
                    {errorMessage}
                </div>

                <div className="mb-3">
                    <label htmlFor="restaurant-name" className="form-label">
                        Name
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="restaurant-name"
                        minLength={1}
                        maxLength={50}
                        aria-describedby="restaurant-name-help"
                        value={name}
                        onChange={(e) => setName(e.currentTarget.value)}
                    />
                    <div className="form-text">
                        Restaurant name should be unique
                    </div>
                    <div className="form-text">
                        Max length 50 symbols ({name.length} / 50)
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="restaurant-name" className="form-label">
                        Description
                    </label>
                    <textarea
                        className={cx(styles.restaurantDescriptionField, "form-control")}
                        id="restaurant-description"
                        minLength={1}
                        maxLength={250}
                        aria-describedby="restaurant-description-help"
                        value={description}
                        onChange={(e) => setDescription(e.currentTarget.value)}
                    />
                    <div className="form-text">
                        Max length 250 symbols ({description.length} / 250)
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="restaurant-image" className="form-label">
                        Image
                    </label>
                    <ImageUploading
                        value={[]}
                        onChange={onImageChange}
                    >
                        {({
                            onImageUpload,
                            isDragging,
                            dragProps
                        }) => (
                            <div>
                                <button
                                    className={cx({
                                        dragging: isDragging
                                    }, "btn btn-outline-primary mb-2 me-2")}
                                    style={isDragging ? { color: "red" } : { }}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        onImageUpload();
                                    }}
                                    {...dragProps}
                                >
                                    {!image ? "Add" : "Update"} image
                                </button>
                                <span id="restaurant-image-help" className="form-text">
                                    Max file size 500kB, preferred 4:3 aspect ratio
                                </span>
                            </div>
                        )}
                    </ImageUploading>
                    {
                        image &&
                        <div data-aspect-ratio="4:3">
                            {/* eslint-disable-next-line @next/next/no-img-element */}
                            <img
                                src={image}
                                alt={name}
                                className="w-100"
                            />
                        </div>
                    }
                </div>
            </form>
        </ModalWrapper>
    );
}

