import React, { useEffect, useState } from "react";

const MAX_USERS_IN_SEARCH_RESULTS = 5;

type Props = {
    users: {
        id: string;
        name: string;
    }[];
    select(id: string): void;
};

export default function UserSearchInput({
    users, select
}: Props) {
    const [searchTerm, setSearchTerm] = useState("");
    const [filteredUsersList, setFilteredUsersList] = useState([] as {
        id: string;
        name: string;
    }[]);

    useEffect(() => {
        const lowerCaseSearchTerm = searchTerm.toLowerCase();
        const newFilteredUsersList = users
            .map((user) => ({
                ...user,
                searchTermPos: user.name
                    .toLowerCase()
                    .indexOf(lowerCaseSearchTerm)
            }))
            .filter((user) => user.searchTermPos !== -1)
            .sort((user1, user2) => (user1.searchTermPos > user2.searchTermPos ? 1 : -1));
        // console.log(searchTerm, users, newFilteredUsersList);
        setFilteredUsersList(newFilteredUsersList);
    }, [users, searchTerm]);

    function selectUser(
        e: React.FormEvent<HTMLButtonElement>,
        userId: string
    ) {
        e.preventDefault();
        e.stopPropagation();
        select(userId);
        setSearchTerm("");
    }

    const filteredUsersListElems = filteredUsersList
        .slice(0, MAX_USERS_IN_SEARCH_RESULTS - 1)
        .map((blockedUser, i) => (
            <button
                key={i}
                className="list-group-item list-group-item-action user-dropdown-elem"
                onClick={(e) => selectUser(e, blockedUser.id)}
            >
                {blockedUser.name}
            </button>
        ));

    return (
        <div className="mt-2 mb-3">
            <div className="input-group flex-nowrap">
                <span className="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                    </svg>
                </span>
                <input
                    type="text"
                    className="form-control"
                    placeholder="Type user name..."
                    aria-label="Search"
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.currentTarget.value)}
                    onBlur={(e) => {
                        const relElement = e.relatedTarget as HTMLElement;
                        if (relElement?.className.indexOf("user-dropdown-elem") === -1) {
                            setSearchTerm("");
                        }
                    }}
                />
            </div>

            <div className="list-group shadow" hidden={!searchTerm}>
                {
                    filteredUsersListElems.length > 0 ? <>
                        {filteredUsersListElems}
                        {
                            filteredUsersList.length > MAX_USERS_IN_SEARCH_RESULTS &&
                            <div className="list-group-item user-dropdown-elem">
                                and {filteredUsersList.length - MAX_USERS_IN_SEARCH_RESULTS} more...
                            </div>
                        }
                    </> : (
                        <div className="list-group-item user-dropdown-elem">
                                no results
                        </div>
                    )
                }
            </div>

        </div>
    );
}

