import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

export default class MockDatabase {

    private static mongoServer: MongoMemoryServer;

    /**
     * Connect to the in-memory database.
     */
    static async connect() {
        this.mongoServer = await MongoMemoryServer.create();
        const uri = this.mongoServer.getUri();
        const mongooseOpts = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            bufferCommands: false,
            autoIndex: true
        };
        await mongoose.connect(uri, mongooseOpts);
    }

    /**
     * Drop database, close the connection and stop mongod.
     */
    static async closeDatabase() {
        await mongoose.connection.close();
        await this.mongoServer.stop();
    }

    /**
     * Remove all the data for all db collections.
     */
    static async clearDatabase() {
        const { collections } = mongoose.connection;
        for (const key in collections) {
            await collections[key].deleteMany({});
        }
    }

}
