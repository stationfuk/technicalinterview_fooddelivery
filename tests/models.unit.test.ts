import MockDatabase from "./MockDatabase";
import restaurantTests from "../models/Restaurant.unit.test.part";
import mealTests from "../models/Meal.unit.test.part";
import userTests from "../models/User.unit.test.part";
import orderTests from "../models/Order.unit.test.part";

jest.setTimeout(60 * 1000);

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll((done) => {
    MockDatabase.connect()
        .then(done);
});

/**
 * Clear all test data after every test.
 */
afterEach((done) => {
    MockDatabase.clearDatabase()
        .then(done);
});

/**
 * Remove and close the db and server.
 */
afterAll((done) => {
    MockDatabase.closeDatabase()
        .then(done);
});

describe("Database models (unit tests)", () => {
    restaurantTests();
    mealTests();
    userTests();
    orderTests();
});
