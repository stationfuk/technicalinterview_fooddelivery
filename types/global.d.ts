import {Mongoose} from "mongoose";

declare global {
    var mongoose: {
        conn?: Mongoose;
        promise?: Promise<Mongoose>;
    }
}

export {};
